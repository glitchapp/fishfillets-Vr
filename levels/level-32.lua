local level = {}


level.name = 'level-32'

-- first cell is map[1][1], top left corner
level.map = 
{	
{1,1,1,1,3,3,3,3,3,3,3,3,3,3,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,3,1,1,3,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,1,1},
{1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,1},
{1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1},
{1,1,1,1,1,1,3,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1},
{1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
{1,1,1,1,1,3,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
{1,1,1,1,3,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,3,3,3,3,3,3,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,1,1,1,3,0,3,1,1,1,1,3,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,3,3,3,1,1,3,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,0,3,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,3,1,1,3,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{1,1,1,3,0,0,0,0,0,3,3,3,3,3,0,0,3,0,0,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{6,6,6,6,6,6,6,1,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{6,6,6,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,3,1,1,3,0,0,0,0,3,1,1},
{3,3,3,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,3,3,3,1,1,1},
{1,1,3,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1},
{1,1,3,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,1,3,0,0,3,3,0,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,1,3,0,3,1,1,3,0,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
{1,1,1,3,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
	{name = 'coral_cerv',
	heavy = false,
	x = 19,
	y = 12,
	form = 
		{
			{1,1,1,1,1,1,1},
			{1,0,0,0,0,0,0},
			{1,1,1,1,1,1,1},
			{1,0,0,0,0,0,0},
			{1,1,1,1,1,1,1},
		},
	},
	{name = 'koral_zel',
	heavy = false,
	x = 25,
	y = 10,
	form = 
		{
			{0,0,1,0,0,0,0,0,0},
			{1,1,1,1,1,1,1,1,1},
			{0,0,1,0,0,0,0,0,1},
			{1,1,1,0,0,0,0,0,1},
			{0,0,1,0,0,0,0,0,1},
			{1,1,1,0,0,0,0,0,1},
		},
	},
	{name = 'koral_bily',
	heavy = false,
	x = 9,
	y = 16,
	form = 
		{
			{0,1,0,0,0,0,0,1,0},
			{1,1,1,1,1,1,1,1,1},
		},
	},
	{name = 'pipe2',
	heavy = true,
	x = 36,
	y = 21,
	form = 
		{
			{1},	
			{1},
			{1},
		},
	},
	{name = 'pipe3',
	heavy = true,
	x = 40,
	y = 23,
	form = 
		{
			{1,1,1,1},
			{0,0,0,1},
			{0,0,0,1},
			{0,0,0,1},
			{0,0,0,1},
		},
	},
	{name = 'pipe4',
	heavy = true,
	x = 12,
	y = 33,
	form = 
		{
			{1,0},
			{1,0},
			{1,1},
			{1,0},
		},
	},
	{name = 'pipe5',
	heavy = true,
	x = 19,
	y = 27,
	form = 
		{
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},			
		},
	},
	{name = 'sasanka_00',
	heavy = false,
	x = 18,
	y = 3,
	form = 
		{
			{0,1},
			{0,1},
		},
	},
	{name = 'pipe',
	heavy = true,
	x = 30,
	y = 9,
	form = 
		{
			{1},
			{1},
		},
	},
	{name = 'snail',
	heavy = false,
	x = 26,
	y = 31,
	form = 
		{
			{1},
		},
	},
	{name = 'musle_troj',
	heavy = false,
	x = 41,
	y = 22,
	form = 
		{
			{1,1,1},
		},
	},
	{name = 'korald',
	heavy = false,
	x = 42,
	y = 32,
	form = 
		{
			{1,1},
			{1,1},
		},
	},
	{name = 'pipe2',
	heavy = true,
	x = 37,
	y = 21,
	form = 
		{
			{1},
			{1},
			{1},
		},
	},
	{name = 'pipe2',
	heavy = true,
	x = 38,
	y = 21,
	form = 
		{
			{1},
			{1},
			{1},
		},
	},
	{name = 'pipe2',
	heavy = true,
	x = 39,
	y = 21,
	form = 
		{
			{1},
			{1},
			{1},
		},
	},
	{name = 'korala',
	heavy = false,
	x = 5,
	y = 10,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
			{0,1,1,0},
			{0,1,1,0},
			{0,1,0,0},
		},
	},
	{name = 'koralb',
	heavy = false,
	x = 6,
	y = 3,
	form = 
		{
			{1,1,1,0,0},
			{1,1,1,0,0},
			{1,1,0,0,0},
			{1,1,1,1,1},
			{1,1,1,1,1},
			{1,1,1,1,1},
		},
	},
{name = 'koralc',
	heavy = false,
	x = 9,
	y = 4,
	form = 
		{
			{0,1,1,0,0},
			{1,1,1,1,1},
			{1,1,1,1,1},
		},
	},
	{name = 'korald',
	heavy = false,
	x = 13,
	y = 3,
	form = 
		{
			{1,1},
			{1,1},
		},
	},
	{name = 'snail',
	heavy = false,
	x = 24,
	y = 4,
	form = 
		{
			{1},
		},
	},
	{name = 'horsefish',
	heavy = false,
	x = 42,
	y = 20,
	form = 
		{
			{1},
			{1},
		},
	},
	{name = 'koral_dlouhy',
	heavy = false,
	x = 23,
	y = 5,
	form = 
		{
			{1,1,1,1,1,1,1,1,1,1,1},
			{0,0,0,0,0,0,0,0,0,0,1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 21,
	y = 13,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 28,
	y = 13,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
