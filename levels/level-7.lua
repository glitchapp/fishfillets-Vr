local level = {}


level.name = 'level-7'

-- first cell is map[1][1], top left corner
level.map = 
{	
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,3,1,1,1,1,1,1,1,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,3,3,3,0,0,0,3,1,1,3,3,3,3,1,1,1,1,1,1,1,1,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,3,3,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3},
{6,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0},
{6,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{6,6,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{6,6,6,3,3,3,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{6,6,6,6,6,0,3,1,1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,1,1,1},
{6,6,6,6,6,0,3,1,1,1,1,1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3},
{6,6,6,6,6,0,0,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{3,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{3,6,6,6,6,0,0,3,0,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1},
{1,3,3,3,3,3,3,1,3,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,3,1,1,1,1},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,1,1,1,1,1},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 4,
		w = 6,
		h = 8,
	}
}

level.blocks = 
{
{name = 'ocel',
	heavy = true,
	x = 21,
	y = 3,
	form = 
		{
			{1,1,1,1,1,0,0,0,0},
			{0,0,0,0,1,0,0,0,0},
			{0,0,0,0,1,1,1,1,1},
		},
	},

{name = 'matrace',
	heavy = false,
	x = 28,
	y = 13,
	form = 
		{
			{1,1,1,1},
		},
	},
	
	{name = 'plz_01',
	heavy = false,
	x = 6,
	y = 12,
	form = 
		{
			{1,1},
			{0,1},
		},
	},
	
	{name = 'snak',
	heavy = false,
	x = 42,
	y = 7,
	form = 
		{
			{1},
		},
	},
	
	{name = 'snak',
	heavy = false,
	x = 44,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'musla',
	heavy = false,
	x = 16,
	y = 11,
	form = 
		{
			{1},
			{1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 45,
	y = 5,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 44,
	y = 11,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
