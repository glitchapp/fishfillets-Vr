local level = {}


level.name = 'level-60'

-- first cell is map[1][1], top left corner
level.map = 
{	
{1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,1,1,1,3,1,1,1,3,6,6,6,6,3,3,0,0,0},
{1,1,3,3,3,3,3,3,1,1,1,3,0,0,0,3,3,3,0,3,1,1,3,6,6,6,6,3,3,0,0,0},
{1,3,0,0,0,0,0,0,3,3,1,3,0,0,0,3,1,0,0,3,3,3,3,6,6,6,6,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,3,3,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,3,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0},
{1,1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,0,0,0},
{1,1,1,1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,0,0,0},
{1,1,0,0,0,0,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0},
{1,1,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,3,6,6,6},
{1,3,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,6,6,6,6,6},
{1,3,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,6,6,6,6,6},
{1,3,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,6,6,6,6,6},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6,6,6},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0},
{1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
	{name = 'krystal_00',
	heavy = false,
	x = 9,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 10,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 11,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 12,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 13,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 14,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 15,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 16,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 17,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 18,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 19,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 20,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 21,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 22,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 23,
	y = 6,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 9,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 10,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 11,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 12,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 13,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 14,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 15,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 16,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 17,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 18,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 19,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 20,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 21,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 22,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 23,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 9,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 10,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 11,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 12,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 13,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 14,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 15,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 16,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 17,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 18,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 19,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 20,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 21,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 22,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 23,
	y = 7,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 9,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 10,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 11,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 12,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 13,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 14,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 15,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 16,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 17,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 18,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 19,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 20,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 21,
	y = 9,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 22,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 23,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 9,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 10,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 11,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 12,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 13,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 14,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 15,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 16,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 17,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 18,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 23,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 9,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 10,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 14,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 15,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 16,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 17,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 18,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 23,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 9,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 10,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = '82',
	heavy = false,
	x = 11,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 12,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 13,
	y = 11,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 14,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 15,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 16,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 17,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 18,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 19,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 20,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 21,
	y = 10,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 23,
	y = 12,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 9,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 10,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 11,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 12,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 13,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 14,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 15,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 16,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_08',
	heavy = false,
	x = 17,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_12',
	heavy = false,
	x = 18,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_16',
	heavy = false,
	x = 19,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_20',
	heavy = false,
	x = 20,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_24',
	heavy = false,
	x = 21,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_00',
	heavy = false,
	x = 22,
	y = 13,
	form = 
		{
			{1},
		},
	},
	{name = 'krystal_04',
	heavy = false,
	x = 23,
	y = 13,
	form = 
		{
			{1},
		},
	},
	
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 11,
	y = 12,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 19,
	y = 11,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
