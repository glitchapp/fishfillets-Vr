function loadlevelassets()

--Players

	fish1tex = lovr.graphics.newTexture("assets/players/fish-4x2.png")
    fish1mat = lovr.graphics.newMaterial(fish1tex,1,1,1,1)
    fish2tex = lovr.graphics.newTexture("assets/players/fish-3x1.png")
    fish2mat = lovr.graphics.newMaterial(fish2tex,1,1,1,1)

	
if nLevel==1 then

	--tablemesh=lovr.graphics.newModel('externalassets/levels/level1/table.glb')
	level1bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level1bordermat = lovr.graphics.newMaterial(level1bordertex,1,1,1,1)
    level1bck1tex=lovr.graphics.newTexture("externalassets/levels/level1/level1bck1.png")
    level1bck1mat = lovr.graphics.newMaterial(level1bck1tex,1,1,1,1)
    level1bck2tex=lovr.graphics.newTexture("externalassets/levels/level1/level1bck2.jpg")
    level1bck2mat = lovr.graphics.newMaterial(level1bck2tex,1,1,1,1)
    level1bck3tex=lovr.graphics.newTexture("externalassets/levels/level1/level1bck3.jpg")
elseif nLevel==2 then
	bottle=lovr.graphics.newModel('externalassets/levels/level2/bottle.glb')
	clawhammer=lovr.graphics.newModel('externalassets/levels/level2/clawhammer.glb')
	level2bck1tex=lovr.graphics.newTexture("externalassets/levels/level2/level2bck.jpg")
    level2bck1mat = lovr.graphics.newMaterial(level2bck1tex,1,1,1,1)
    level2bck2tex=lovr.graphics.newTexture("externalassets/levels/level2/level2bck2.jpg")
    level2bck2mat = lovr.graphics.newMaterial(level2bck2tex,1,1,1,1)
    level2bordertex=lovr.graphics.newTexture("externalassets/levels/level2/block.jpg")
    level2block = lovr.graphics.newMaterial(level2bordertex,1,1,1,1)
    
elseif nLevel==3 then
	axe=lovr.graphics.newModel('externalassets/levels/level3/axe.glb')
	oillamp=lovr.graphics.newModel('externalassets/levels/level3/oillamp.glb')
	level3bordertex=lovr.graphics.newTexture("externalassets/levels/level3/level3frg.png")
    level3bordermat = lovr.graphics.newMaterial(level3bordertex,1,1,1,1)
    --level3bck3tex=lovr.graphics.newTexture("externalassets/levels/level1/level1bck1.png")
    --level3bck3mat = lovr.graphics.newMaterial(level1bck1tex,1,1,1,1)
    level3bcktex=lovr.graphics.newTexture("externalassets/levels/level3/level3bck.jpg")
    level3bckmat = lovr.graphics.newMaterial(level3bcktex,1,1,1,1)
    level3bck2tex=lovr.graphics.newTexture("externalassets/levels/level3/level3bck2.jpg")
    level3bck2mat = lovr.graphics.newMaterial(level3bck2tex,1,1,1,1)
elseif nLevel==4 then
  	--Book model
	book=lovr.graphics.newModel('externalassets/levels/level4/book.glb')
	level4bcktex=lovr.graphics.newTexture("externalassets/levels/level4/level4bck1.jpg")
    level4bckmat = lovr.graphics.newMaterial(level4bcktex,1,1,1,1)
    level4bck2tex=lovr.graphics.newTexture("externalassets/levels/level4/level4bck2.jpg")
    level4bckmat = lovr.graphics.newMaterial(level4bcktex,1,1,1,1)
    level4bordertex=lovr.graphics.newTexture("externalassets/levels/level4/block.jpg")
    level4block = lovr.graphics.newMaterial(level4bordertex,1,1,1,1)
elseif nLevel==5 then
  	
	level5bcktex=lovr.graphics.newTexture("externalassets/levels/level5/level5bck.jpg")
    level5bckmat = lovr.graphics.newMaterial(level5bcktex,1,1,1,1)
    level5bck2tex=lovr.graphics.newTexture("externalassets/levels/level5/level5bck2.jpg")
    level5bckmat = lovr.graphics.newMaterial(level5bcktex,1,1,1,1)
    level5bordertex=lovr.graphics.newTexture("externalassets/levels/level5/block.jpg")
    level5block = lovr.graphics.newMaterial(level5bordertex,1,1,1,1)

elseif nLevel==6 then
  	
  	broom=lovr.graphics.newModel('externalassets/levels/level6/broom.glb')
	level6bcktex=lovr.graphics.newTexture("externalassets/levels/level6/level6bck.jpg")
    level6bckmat = lovr.graphics.newMaterial(level6bcktex,1,1,1,1)
    level6bck2tex=lovr.graphics.newTexture("externalassets/levels/level3/level3bck2.jpg")
    level6bckmat = lovr.graphics.newMaterial(level6bcktex,1,1,1,1)
    level6bordertex=lovr.graphics.newTexture("externalassets/levels/level6/block.jpg")
    level6block = lovr.graphics.newMaterial(level6bordertex,1,1,1,1)

elseif nLevel==7 then
  	
	level7bcktex=lovr.graphics.newTexture("externalassets/levels/level7/level7bck.jpg")
    level7bckmat = lovr.graphics.newMaterial(level7bcktex,1,1,1,1)
    level7bck2tex=lovr.graphics.newTexture("externalassets/levels/level7/level7bck.jpg")
    level7bckmat = lovr.graphics.newMaterial(level7bcktex,1,1,1,1)
    level7bordertex=lovr.graphics.newTexture("externalassets/levels/level7/block.jpg")
    level7block = lovr.graphics.newMaterial(level7bordertex,1,1,1,1)
    
elseif nLevel==8 then
  	
  	wc=lovr.graphics.newModel('externalassets/levels/level8/toilet.glb')
	level8bcktex=lovr.graphics.newTexture("externalassets/levels/level8/level8bck.jpg")
    level8bckmat = lovr.graphics.newMaterial(level8bcktex,1,1,1,1)
    level8bck2tex=lovr.graphics.newTexture("externalassets/levels/level3/level3bck2.jpg")
    level8bckmat = lovr.graphics.newMaterial(level8bcktex,1,1,1,1)
    level8bordertex=lovr.graphics.newTexture("externalassets/levels/level8/block.jpg")
    level8block = lovr.graphics.newMaterial(level8bordertex,1,1,1,1)
    
elseif nLevel==9 then
  	
	level9bcktex=lovr.graphics.newTexture("externalassets/levels/level9/level9bck.jpg")
    level9bckmat = lovr.graphics.newMaterial(level9bcktex,1,1,1,1)
    level9bck2tex=lovr.graphics.newTexture("externalassets/levels/level9/level9bck2.jpg")
    level9bckmat = lovr.graphics.newMaterial(level9bcktex,1,1,1,1)
    level9bordertex=lovr.graphics.newTexture("externalassets/levels/level9/block.jpg")
    level9block = lovr.graphics.newMaterial(level9bordertex,1,1,1,1)

elseif nLevel==10 then
  	
  	cruiseship=lovr.graphics.newModel('externalassets/levels/level10/CruiseShip.glb')
	level10bcktex=lovr.graphics.newTexture("externalassets/levels/level10/level10bck.jpg")
    level10bckmat = lovr.graphics.newMaterial(level10bcktex,1,1,1,1)
    level10bck2tex=lovr.graphics.newTexture("externalassets/levels/level10/level10bck.jpg")
    level10bckmat = lovr.graphics.newMaterial(level10bcktex,1,1,1,1)
    level10bordertex=lovr.graphics.newTexture("externalassets/levels/level10/block.jpg")
    level10block = lovr.graphics.newMaterial(level10bordertex,1,1,1,1)
elseif nLevel==11 then
	level11bordertex=lovr.graphics.newTexture("externalassets/levels/level11/level11frg1.jpg")
    level11bordermat = lovr.graphics.newMaterial(level11bordertex,1,1,1,1)
    level11bcktex=lovr.graphics.newTexture("externalassets/levels/level11/level11bck.jpg")
    level11bckmat = lovr.graphics.newMaterial(level11bcktex,1,1,1,1)
    	--3d elk model
	elk=lovr.graphics.newModel('externalassets/levels/level11/elk.glb')

elseif nLevel==12 then
  	
	level12bcktex=lovr.graphics.newTexture("externalassets/levels/level12/level12bck.jpg")
    level12bckmat = lovr.graphics.newMaterial(level12bcktex,1,1,1,1)
    level12bck2tex=lovr.graphics.newTexture("externalassets/levels/level12/level12bck.jpg")
    level12bckmat = lovr.graphics.newMaterial(level12bcktex,1,1,1,1)
    level12bordertex=lovr.graphics.newTexture("externalassets/levels/level12/block.jpg")
    level12block = lovr.graphics.newMaterial(level12bordertex,1,1,1,1)
    --Galleon=lovr.graphics.newModel('externalassets/levels/level12/GalleonOGA.glb')
    
elseif nLevel==13 then
  	
  	skull=lovr.graphics.newModel('externalassets/levels/level13/skullfront.glb')
	level13bcktex=lovr.graphics.newTexture("externalassets/levels/level13/level13bck.jpg")
    level13bckmat = lovr.graphics.newMaterial(level13bcktex,1,1,1,1)
    level13bck2tex=lovr.graphics.newTexture("externalassets/levels/level13/level13bck.jpg")
    level13bckmat = lovr.graphics.newMaterial(level13bcktex,1,1,1,1)
    level13bordertex=lovr.graphics.newTexture("externalassets/levels/level13/block.jpg")
    level13block = lovr.graphics.newMaterial(level13bordertex,1,1,1,1)

elseif nLevel==14 then
  	
	level14bcktex=lovr.graphics.newTexture("externalassets/levels/level10/level10bck.jpg")
    level14bckmat = lovr.graphics.newMaterial(level14bcktex,1,1,1,1)
    level14bck2tex=lovr.graphics.newTexture("externalassets/levels/level14/level14bck.jpg")
    level14bckmat = lovr.graphics.newMaterial(level14bck2tex,1,1,1,1)
    level14bordertex=lovr.graphics.newTexture("externalassets/levels/level14/block.jpg")
    level14block = lovr.graphics.newMaterial(level14bordertex,1,1,1,1)
    
    
    elseif nLevel==15 then
  	
  	phonebase=lovr.graphics.newModel('externalassets/levels/level15/rotaryphonebase.glb')
  	handset=lovr.graphics.newModel('externalassets/levels/level15/rotaryphonehandset.glb')
  	microscope=lovr.graphics.newModel('externalassets/levels/level15/microscope.glb')
  	
	level15bcktex=lovr.graphics.newTexture("externalassets/levels/level15/level15bck.jpg")
    level15bckmat = lovr.graphics.newMaterial(level15bcktex,1,1,1,1)
    level15bck2tex=lovr.graphics.newTexture("externalassets/levels/level15/level15bck2.jpg")
    level15bckmat = lovr.graphics.newMaterial(level15bck2tex,1,1,1,1)
    level15bordertex=lovr.graphics.newTexture("externalassets/levels/level15/block.jpg")
    level15block = lovr.graphics.newMaterial(level15bordertex,1,1,1,1)
    
    elseif nLevel==16 then
  	
	level16bcktex=lovr.graphics.newTexture("externalassets/levels/level11/level11bck.jpg")
    level16bckmat = lovr.graphics.newMaterial(level16bcktex,1,1,1,1)
    level16bck2tex=lovr.graphics.newTexture("externalassets/levels/level16/level16bck.jpg")
    level16bckmat = lovr.graphics.newMaterial(level16bck2tex,1,1,1,1)
    level16bordertex=lovr.graphics.newTexture("externalassets/levels/level16/block.jpg")
    level16block = lovr.graphics.newMaterial(level16bordertex,1,1,1,1)
    
    elseif nLevel==17 then
  	
	level17bcktex=lovr.graphics.newTexture("externalassets/levels/level17/level17bck.jpg")
    level17bckmat = lovr.graphics.newMaterial(level17bcktex,1,1,1,1)
    level17bck2tex=lovr.graphics.newTexture("externalassets/levels/level17/level17bck.jpg")
    level17bckmat = lovr.graphics.newMaterial(level17bcktex,1,1,1,1)
    level17bordertex=lovr.graphics.newTexture("externalassets/levels/level17/block.jpg")
    level17block = lovr.graphics.newMaterial(level17bordertex,1,1,1,1)
    
    
    elseif nLevel==18 then
  	
	level18bcktex=lovr.graphics.newTexture("externalassets/levels/level10/level10bck.jpg")
    level18bckmat = lovr.graphics.newMaterial(level18bcktex,1,1,1,1)
    level18bck2tex=lovr.graphics.newTexture("externalassets/levels/level10/level10bck.jpg")
    level18bckmat = lovr.graphics.newMaterial(level18bcktex,1,1,1,1)
    level18bordertex=lovr.graphics.newTexture("externalassets/levels/level10/block.jpg")
    level18block = lovr.graphics.newMaterial(level18bordertex,1,1,1,1)
    
    elseif nLevel==19 then
  	
	level19bcktex=lovr.graphics.newTexture("externalassets/levels/level13/level13bck.jpg")
    level19bckmat = lovr.graphics.newMaterial(level19bcktex,1,1,1,1)
    level19bck2tex=lovr.graphics.newTexture("externalassets/levels/level10/level10bck.jpg")
    level19bckmat = lovr.graphics.newMaterial(level19bcktex,1,1,1,1)
    level19bordertex=lovr.graphics.newTexture("externalassets/levels/level19/block.jpg")
    level19block = lovr.graphics.newMaterial(level19bordertex,1,1,1,1)
    
    elseif nLevel==20 then
  	shell=lovr.graphics.newModel('externalassets/levels/level20/shell.glb')
  	skull=lovr.graphics.newModel('externalassets/levels/level20/skull.glb')
	level20bcktex=lovr.graphics.newTexture("externalassets/levels/level20/level20bck.jpg")
    level20bckmat = lovr.graphics.newMaterial(level20bcktex,1,1,1,1)
    level20bck2tex=lovr.graphics.newTexture("externalassets/levels/level20/level20bck.jpg")
    level20bckmat = lovr.graphics.newMaterial(level20bcktex,1,1,1,1)
    level20bordertex=lovr.graphics.newTexture("externalassets/levels/level20/block.jpg")
    level20block = lovr.graphics.newMaterial(level20bordertex,1,1,1,1)
    
        elseif nLevel==21 then
  	
	level21bcktex=lovr.graphics.newTexture("externalassets/levels/level21/level21bck.jpg")
    level21bckmat = lovr.graphics.newMaterial(level21bcktex,1,1,1,1)
    level21bck2tex=lovr.graphics.newTexture("externalassets/levels/level21/level21bck.jpg")
    level21bckmat = lovr.graphics.newMaterial(level21bcktex,1,1,1,1)
    level21bordertex=lovr.graphics.newTexture("externalassets/levels/level21/block.jpg")
    level21block = lovr.graphics.newMaterial(level21bordertex,1,1,1,1)
    
        elseif nLevel==22 then
  	
	level22bcktex=lovr.graphics.newTexture("externalassets/levels/level22/level22bck.jpg")
    level22bckmat = lovr.graphics.newMaterial(level22bcktex,1,1,1,1)
    level22bck2tex=lovr.graphics.newTexture("externalassets/levels/level22/level22bck.jpg")
    level22bckmat = lovr.graphics.newMaterial(level22bcktex,1,1,1,1)
    level22bordertex=lovr.graphics.newTexture("externalassets/levels/level22/block.jpg")
    level22block = lovr.graphics.newMaterial(level22bordertex,1,1,1,1)
    
        elseif nLevel==23 then
  	
	level23bcktex=lovr.graphics.newTexture("externalassets/levels/level23/level23bck.jpg")
    level23bckmat = lovr.graphics.newMaterial(level23bcktex,1,1,1,1)
    level23bck2tex=lovr.graphics.newTexture("externalassets/levels/level23/level23bck.jpg")
    level23bckmat = lovr.graphics.newMaterial(level23bcktex,1,1,1,1)
    level23bordertex=lovr.graphics.newTexture("externalassets/levels/level23/block.jpg")
    level23block = lovr.graphics.newMaterial(level23bordertex,1,1,1,1)
    
        elseif nLevel==24 then
  	
	level24bcktex=lovr.graphics.newTexture("externalassets/levels/level24/level24bck.jpg")
    level24bckmat = lovr.graphics.newMaterial(level24bcktex,1,1,1,1)
    level24bck2tex=lovr.graphics.newTexture("externalassets/levels/level24/level24bck.jpg")
    level24bckmat = lovr.graphics.newMaterial(level24bcktex,1,1,1,1)
    level24bordertex=lovr.graphics.newTexture("externalassets/levels/level24/block.jpg")
    level24block = lovr.graphics.newMaterial(level24bordertex,1,1,1,1)
    
        elseif nLevel==25 then
  	
	level25bcktex=lovr.graphics.newTexture("externalassets/levels/level25/level25bck2.jpg")
    level25bckmat = lovr.graphics.newMaterial(level25bcktex,1,1,1,1)
    level25bck2tex=lovr.graphics.newTexture("externalassets/levels/level25/level25bck.jpg")
    level25bck2mat = lovr.graphics.newMaterial(level25bck2tex,1,1,1,1)
    level25bordertex=lovr.graphics.newTexture("externalassets/levels/level25/block.jpg")
    level25block = lovr.graphics.newMaterial(level25bordertex,1,1,1,1)
    
        elseif nLevel==26 then
  	
	level26bcktex=lovr.graphics.newTexture("externalassets/levels/level26/level26bck.jpg")
    level26bckmat = lovr.graphics.newMaterial(level26bcktex,1,1,1,1)
    level26bck2tex=lovr.graphics.newTexture("externalassets/levels/level26/level26bck.jpg")
    level26bckmat = lovr.graphics.newMaterial(level26bcktex,1,1,1,1)
    level26bordertex=lovr.graphics.newTexture("externalassets/levels/level26/block.jpg")
    level26block = lovr.graphics.newMaterial(level26bordertex,1,1,1,1)
    
        elseif nLevel==27 then
  	
	level27bcktex=lovr.graphics.newTexture("externalassets/levels/level27/level27bck.jpg")
    level27bckmat = lovr.graphics.newMaterial(level27bcktex,1,1,1,1)
    level27bck2tex=lovr.graphics.newTexture("externalassets/levels/level27/level27bck.jpg")
    level27bckmat = lovr.graphics.newMaterial(level27bcktex,1,1,1,1)
    level27bordertex=lovr.graphics.newTexture("externalassets/levels/level27/block.jpg")
    level27block = lovr.graphics.newMaterial(level27bordertex,1,1,1,1)
    
        elseif nLevel==28 then
  	
	level28bcktex=lovr.graphics.newTexture("externalassets/levels/level28/level28bck.jpg")
    level28bckmat = lovr.graphics.newMaterial(level28bcktex,1,1,1,1)
    level28bck2tex=lovr.graphics.newTexture("externalassets/levels/level28/level28bck.jpg")
    level28bckmat = lovr.graphics.newMaterial(level28bcktex,1,1,1,1)
    level28bordertex=lovr.graphics.newTexture("externalassets/levels/level28/block.jpg")
    level28block = lovr.graphics.newMaterial(level28bordertex,1,1,1,1)
    
        elseif nLevel==29 then
  	
	level29bcktex=lovr.graphics.newTexture("externalassets/levels/level24/level24bck.jpg")
    level29bckmat = lovr.graphics.newMaterial(level29bcktex,1,1,1,1)
    level29bck2tex=lovr.graphics.newTexture("externalassets/levels/level20/level20bck.jpg")
    level29bckmat = lovr.graphics.newMaterial(level29bcktex,1,1,1,1)
    level29bordertex=lovr.graphics.newTexture("externalassets/levels/level20/block.jpg")
    level29block = lovr.graphics.newMaterial(level29bordertex,1,1,1,1)
    
elseif nLevel==30 then
	level30bordertex=lovr.graphics.newTexture("externalassets/levels/level30/level30bck2.jpg")
    level30bordermat = lovr.graphics.newMaterial(level30bordertex,1,1,1,1)
	level30bcktex=lovr.graphics.newTexture("externalassets/levels/level30/level30bck.jpg")
	level30bckmat = lovr.graphics.newMaterial(level30bcktex,1,1,1,1)
	level30bordertex=lovr.graphics.newTexture("externalassets/levels/level30/block2.jpg")
    level30block = lovr.graphics.newMaterial(level30bordertex,1,1,1,1)
	
	
elseif nLevel==31 then
	level31bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level31bordermat = lovr.graphics.newMaterial(level31bordertex,1,1,1,1)
	level31bcktex=lovr.graphics.newTexture("externalassets/levels/level31/level31bck.jpg")
	level31bckmat = lovr.graphics.newMaterial(level31bcktex,1,1,1,1)
	level31bordertex=lovr.graphics.newTexture("externalassets/levels/level30/block1.jpg")
    level31block = lovr.graphics.newMaterial(level31bordertex,1,1,1,1)
	
	    elseif nLevel==32 then
  	
	level32bcktex=lovr.graphics.newTexture("externalassets/levels/level32/level32bck.jpg")
    level32bckmat = lovr.graphics.newMaterial(level32bcktex,1,1,1,1)
    level32bck2tex=lovr.graphics.newTexture("externalassets/levels/level32/level32bck2.jpg")
    level32bckmat = lovr.graphics.newMaterial(level32bcktex,1,1,1,1)
    level32bordertex=lovr.graphics.newTexture("externalassets/levels/level32/block.jpg")
    level32block = lovr.graphics.newMaterial(level32bordertex,1,1,1,1)
    
        elseif nLevel==33 then
  	
	level33bcktex=lovr.graphics.newTexture("externalassets/levels/level33/level33bck.jpg")
    level33bckmat = lovr.graphics.newMaterial(level33bcktex,1,1,1,1)
    level33bck2tex=lovr.graphics.newTexture("externalassets/levels/level33/level33bck.jpg")
    level33bckmat = lovr.graphics.newMaterial(level33bcktex,1,1,1,1)
    level33bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level33block = lovr.graphics.newMaterial(level33bordertex,1,1,1,1)
elseif nLevel==34 then
	level34bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level34bordermat = lovr.graphics.newMaterial(level34bordertex,1,1,1,1)
	level34bcktex=lovr.graphics.newTexture("externalassets/levels/level34/level34bck.jpg")
	level34bckmat = lovr.graphics.newMaterial(level34bcktex,1,1,1,1)
	level34bordertex=lovr.graphics.newTexture("externalassets/levels/level34/block.jpg")
    level34block = lovr.graphics.newMaterial(level34bordertex,1,1,1,1)
	    elseif nLevel==35 then
  	
	level35bcktex=lovr.graphics.newTexture("externalassets/levels/level35/level35bck.jpg")
    level35bckmat = lovr.graphics.newMaterial(level35bcktex,1,1,1,1)
    level35bck2tex=lovr.graphics.newTexture("externalassets/levels/level35/level35bck.jpg")
    level35bckmat = lovr.graphics.newMaterial(level35bcktex,1,1,1,1)
    level35bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level35block = lovr.graphics.newMaterial(level35bordertex,1,1,1,1)
    
        elseif nLevel==36 then
  	
	level36bcktex=lovr.graphics.newTexture("externalassets/levels/level36/level36bck.jpg")
    level36bckmat = lovr.graphics.newMaterial(level36bcktex,1,1,1,1)
    level36bck2tex=lovr.graphics.newTexture("externalassets/levels/level36/level36bck.jpg")
    level36bckmat = lovr.graphics.newMaterial(level36bcktex,1,1,1,1)
    level36bordertex=lovr.graphics.newTexture("externalassets/levels/level36/block.jpg")
    level36block = lovr.graphics.newMaterial(level36bordertex,1,1,1,1)
    
        elseif nLevel==37 then
  	
	level37bcktex=lovr.graphics.newTexture("externalassets/levels/level37/level37bck.jpg")
    level37bckmat = lovr.graphics.newMaterial(level37bcktex,1,1,1,1)
    level37bck2tex=lovr.graphics.newTexture("externalassets/levels/level37/level37bck.jpg")
    level37bckmat = lovr.graphics.newMaterial(level37bcktex,1,1,1,1)
    level37bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level37block = lovr.graphics.newMaterial(level37bordertex,1,1,1,1)
    
        elseif nLevel==38 then
  	
  	computer=lovr.graphics.newModel('externalassets/levels/level38/computer.glb')
	level38bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level38bckmat = lovr.graphics.newMaterial(level38bcktex,1,1,1,1)
    level38bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level38bckmat = lovr.graphics.newMaterial(level38bcktex,1,1,1,1)
    level38bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level38block = lovr.graphics.newMaterial(level38bordertex,1,1,1,1)
    
        elseif nLevel==39 then
  	
	level39bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level39bckmat = lovr.graphics.newMaterial(level39bcktex,1,1,1,1)
    level39bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level39bckmat = lovr.graphics.newMaterial(level39bcktex,1,1,1,1)
    level39bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level39block = lovr.graphics.newMaterial(level39bordertex,1,1,1,1)
    
        elseif nLevel==40 then
  	
	level40bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level40bckmat = lovr.graphics.newMaterial(level40bcktex,1,1,1,1)
    level40bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level40bckmat = lovr.graphics.newMaterial(level40bcktex,1,1,1,1)
    level40bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level40block = lovr.graphics.newMaterial(level40bordertex,1,1,1,1)
    
        elseif nLevel==41 then
  	
	level41bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level41bckmat = lovr.graphics.newMaterial(level41bcktex,1,1,1,1)
    level41bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level41bckmat = lovr.graphics.newMaterial(level41bcktex,1,1,1,1)
    level41bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level41block = lovr.graphics.newMaterial(level41bordertex,1,1,1,1)
    
        elseif nLevel==42 then
  	
	level42bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level42bckmat = lovr.graphics.newMaterial(level42bcktex,1,1,1,1)
    level42bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level42bckmat = lovr.graphics.newMaterial(level42bcktex,1,1,1,1)
    level42bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level42block = lovr.graphics.newMaterial(level42bordertex,1,1,1,1)
    
        elseif nLevel==43 then
  	
	level43bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level43bckmat = lovr.graphics.newMaterial(level43bcktex,1,1,1,1)
    level43bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level43bckmat = lovr.graphics.newMaterial(level43bcktex,1,1,1,1)
    level43bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level43block = lovr.graphics.newMaterial(level43bordertex,1,1,1,1)
    
        elseif nLevel==44 then
  	
	level44bcktex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level44bckmat = lovr.graphics.newMaterial(level44bcktex,1,1,1,1)
    level44bck2tex=lovr.graphics.newTexture("externalassets/levels/level38/level38bck.jpg")
    level44bckmat = lovr.graphics.newMaterial(level44bcktex,1,1,1,1)
    level44bordertex=lovr.graphics.newTexture("externalassets/levels/level38/block.jpg")
    level44block = lovr.graphics.newMaterial(level44bordertex,1,1,1,1)
    
        elseif nLevel==45 then
  	
	level45bcktex=lovr.graphics.newTexture("externalassets/levels/level45/level45bck.jpg")
    level45bckmat = lovr.graphics.newMaterial(level45bcktex,1,1,1,1)
    level45bck2tex=lovr.graphics.newTexture("externalassets/levels/level45/level45bck.jpg")
    level45bckmat = lovr.graphics.newMaterial(level45bcktex,1,1,1,1)
    level45bordertex=lovr.graphics.newTexture("externalassets/levels/level45/block.jpg")
    level45block = lovr.graphics.newMaterial(level45bordertex,1,1,1,1)
    
    
        elseif nLevel==46 then
  	--snowman
  	--table=lovr.graphics.newModel('externalassets/levels/level46/table.glb')
  	snowman=lovr.graphics.newModel('externalassets/levels/level46/snowman.glb')
	level46bcktex=lovr.graphics.newTexture("externalassets/levels/level46/level46bck.jpg")
    level46bckmat = lovr.graphics.newMaterial(level46bcktex,1,1,1,1)
    level46bck2tex=lovr.graphics.newTexture("externalassets/levels/level46/level46bck.jpg")
    level46bckmat = lovr.graphics.newMaterial(level46bcktex,1,1,1,1)
    level46bordertex=lovr.graphics.newTexture("externalassets/levels/level46/block.jpg")
    level46block = lovr.graphics.newMaterial(level46bordertex,1,1,1,1)
    
    
        elseif nLevel==47 then
  	
  	cannon=lovr.graphics.newModel('externalassets/levels/level47/cannon.glb')
	level47bcktex=lovr.graphics.newTexture("externalassets/levels/level47/level47bck.jpg")
    level47bckmat = lovr.graphics.newMaterial(level47bcktex,1,1,1,1)
    level47bck2tex=lovr.graphics.newTexture("externalassets/levels/level47/level47bck.jpg")
    level47bckmat = lovr.graphics.newMaterial(level47bcktex,1,1,1,1)
    level47bordertex=lovr.graphics.newTexture("externalassets/levels/level47/block.jpg")
    level47block = lovr.graphics.newMaterial(level47bordertex,1,1,1,1)
    
    
        elseif nLevel==48 then
  	
	level48bcktex=lovr.graphics.newTexture("externalassets/levels/level48/level48bck.jpg")
    level48bckmat = lovr.graphics.newMaterial(level48bcktex,1,1,1,1)
    level48bck2tex=lovr.graphics.newTexture("externalassets/levels/level48/level48bck.jpg")
    level48bckmat = lovr.graphics.newMaterial(level48bcktex,1,1,1,1)
    level48bordertex=lovr.graphics.newTexture("externalassets/levels/level48/block.jpg")
    level48block = lovr.graphics.newMaterial(level48bordertex,1,1,1,1)
    
    
        elseif nLevel==49 then
  	
	level49bcktex=lovr.graphics.newTexture("externalassets/levels/level49/level49bck.jpg")
    level49bckmat = lovr.graphics.newMaterial(level49bcktex,1,1,1,1)
    level49bck2tex=lovr.graphics.newTexture("externalassets/levels/level49/level49bck.jpg")
    level49bckmat = lovr.graphics.newMaterial(level49bcktex,1,1,1,1)
    level49bordertex=lovr.graphics.newTexture("externalassets/levels/level49/block.jpg")
    level49block = lovr.graphics.newMaterial(level49bordertex,1,1,1,1)
    
        elseif nLevel==50 then
  	
  	skull=lovr.graphics.newModel('externalassets/levels/level50/skull.glb')
  	sessel=lovr.graphics.newModel('externalassets/levels/level50/sessel.glb')
	level50bcktex=lovr.graphics.newTexture("externalassets/levels/level50/level50bck.jpg")
    level50bckmat = lovr.graphics.newMaterial(level50bcktex,1,1,1,1)
    level50bck2tex=lovr.graphics.newTexture("externalassets/levels/level50/level50bck.jpg")
    level50bckmat = lovr.graphics.newMaterial(level50bcktex,1,1,1,1)
    level50bordertex=lovr.graphics.newTexture("externalassets/levels/level50/block.jpg")
    level50block = lovr.graphics.newMaterial(level50bordertex,1,1,1,1)
    
        elseif nLevel==51 then
  	
	level51bcktex=lovr.graphics.newTexture("externalassets/levels/level51/level51bck.jpg")
    level51bckmat = lovr.graphics.newMaterial(level51bcktex,1,1,1,1)
    level51bck2tex=lovr.graphics.newTexture("externalassets/levels/level51/level51bck.jpg")
    level51bckmat = lovr.graphics.newMaterial(level51bcktex,1,1,1,1)
    level51bordertex=lovr.graphics.newTexture("externalassets/levels/level51/block.jpg")
    level51block = lovr.graphics.newMaterial(level51bordertex,1,1,1,1)
	
elseif nLevel==52 then
	level52bordertex=lovr.graphics.newTexture("externalassets/levels/level52/level52frg.jpg")
    level52bordermat = lovr.graphics.newMaterial(level52bordertex,1,1,1,1)
    level52bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level52bckmat = lovr.graphics.newMaterial(level52bcktex,1,1,1,1)
    level52bordertex=lovr.graphics.newTexture("externalassets/levels/level52/block.jpg")
    level52block = lovr.graphics.newMaterial(level52bordertex,1,1,1,1)
    
        elseif nLevel==53 then
  	
	level53bcktex=lovr.graphics.newTexture("externalassets/levels/level53/level53bck.jpg")
    level53bckmat = lovr.graphics.newMaterial(level53bcktex,1,1,1,1)
    level53bck2tex=lovr.graphics.newTexture("externalassets/levels/level53/level53bck.jpg")
    level53bckmat = lovr.graphics.newMaterial(level53bcktex,1,1,1,1)
    level53bordertex=lovr.graphics.newTexture("externalassets/levels/level53/block.jpg")
    level53block = lovr.graphics.newMaterial(level53bordertex,1,1,1,1)
        elseif nLevel==54 then
  	
	level54bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level54bckmat = lovr.graphics.newMaterial(level54bcktex,1,1,1,1)
    level54bck2tex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level54bckmat = lovr.graphics.newMaterial(level54bcktex,1,1,1,1)
    level54bordertex=lovr.graphics.newTexture("externalassets/levels/level54/block.jpg")
    level54block = lovr.graphics.newMaterial(level54bordertex,1,1,1,1)
        elseif nLevel==55 then
  	
	level55bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level55bckmat = lovr.graphics.newMaterial(level55bcktex,1,1,1,1)
    level55bck2tex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level55bckmat = lovr.graphics.newMaterial(level55bcktex,1,1,1,1)
    level55bordertex=lovr.graphics.newTexture("externalassets/levels/level55/block.jpg")
    level55block = lovr.graphics.newMaterial(level55bordertex,1,1,1,1)
        elseif nLevel==56 then
  	
	level56bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level56bckmat = lovr.graphics.newMaterial(level56bcktex,1,1,1,1)
    level56bck2tex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level56bckmat = lovr.graphics.newMaterial(level56bcktex,1,1,1,1)
    level56bordertex=lovr.graphics.newTexture("externalassets/levels/level56/block.jpg")
    level56block = lovr.graphics.newMaterial(level56bordertex,1,1,1,1)
        elseif nLevel==57 then
  	
	level57bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level57bckmat = lovr.graphics.newMaterial(level57bcktex,1,1,1,1)
    level57bck2tex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level57bckmat = lovr.graphics.newMaterial(level57bcktex,1,1,1,1)
    level57bordertex=lovr.graphics.newTexture("externalassets/levels/level57/block.jpg")
    level57block = lovr.graphics.newMaterial(level57bordertex,1,1,1,1)
        elseif nLevel==58 then
  	
	level58bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level58bckmat = lovr.graphics.newMaterial(level58bcktex,1,1,1,1)
    level58bck2tex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level58bckmat = lovr.graphics.newMaterial(level58bcktex,1,1,1,1)
    level58bordertex=lovr.graphics.newTexture("externalassets/levels/level58/block.jpg")
    level58block = lovr.graphics.newMaterial(level58bordertex,1,1,1,1)
        elseif nLevel==59 then
  	
	level59bcktex=lovr.graphics.newTexture("externalassets/levels/level59/level59bck.jpg")
    level59bckmat = lovr.graphics.newMaterial(level59bcktex,1,1,1,1)
    level59bck2tex=lovr.graphics.newTexture("externalassets/levels/level59/level59bck.jpg")
    level59bckmat = lovr.graphics.newMaterial(level59bcktex,1,1,1,1)
    level59bordertex=lovr.graphics.newTexture("externalassets/levels/level59/block.jpg")
    level59block = lovr.graphics.newMaterial(level59bordertex,1,1,1,1)
        elseif nLevel==60 then
  	
	level60bcktex=lovr.graphics.newTexture("externalassets/levels/level60/level60bck.jpg")
    level60bckmat = lovr.graphics.newMaterial(level60bcktex,1,1,1,1)
    level60bck2tex=lovr.graphics.newTexture("externalassets/levels/level60/level60bck.jpg")
    level60bckmat = lovr.graphics.newMaterial(level60bcktex,1,1,1,1)
    level60bordertex=lovr.graphics.newTexture("externalassets/levels/level60/block.jpg")
    level60block = lovr.graphics.newMaterial(level60bordertex,1,1,1,1)
	
	elseif nLevel==61 then
	--goblet=lovr.graphics.newModel('externalassets/levels/level61/Goblet.obj')
	level61bordertex=lovr.graphics.newTexture("externalassets/levels/level1/level1frg.png")
    level61bordermat = lovr.graphics.newMaterial(level61bordertex,1,1,1,1)
	level61bcktex=lovr.graphics.newTexture("externalassets/levels/level61/level61bck.jpg")
	level61bckmat = lovr.graphics.newMaterial(level61bcktex,1,1,1,1)
	level61bordertex=lovr.graphics.newTexture("externalassets/levels/level60/block.jpg")
    level61block = lovr.graphics.newMaterial(level61bordertex,1,1,1,1)
	
	
	    elseif nLevel==62 then
  	
	level62bcktex=lovr.graphics.newTexture("externalassets/levels/level62/level62bck.jpg")
    level62bckmat = lovr.graphics.newMaterial(level62bcktex,1,1,1,1)
    level62bck2tex=lovr.graphics.newTexture("externalassets/levels/level62/level62bck.jpg")
    level62bckmat = lovr.graphics.newMaterial(level62bcktex,1,1,1,1)
    level62bordertex=lovr.graphics.newTexture("externalassets/levels/level62/block.jpg")
    level62block = lovr.graphics.newMaterial(level62bordertex,1,1,1,1)
    
        elseif nLevel==63 then
  	
	level63bcktex=lovr.graphics.newTexture("externalassets/levels/level63/level63bck.jpg")
    level63bckmat = lovr.graphics.newMaterial(level63bcktex,1,1,1,1)
    level63bck2tex=lovr.graphics.newTexture("externalassets/levels/level63/level63bck.jpg")
    level63bckmat = lovr.graphics.newMaterial(level63bcktex,1,1,1,1)
    level63bordertex=lovr.graphics.newTexture("externalassets/levels/level63/block.jpg")
    level63block = lovr.graphics.newMaterial(level63bordertex,1,1,1,1)
    
        elseif nLevel==64 then
  	
	level64bcktex=lovr.graphics.newTexture("externalassets/levels/level63/level63bck.jpg")
    level64bckmat = lovr.graphics.newMaterial(level64bcktex,1,1,1,1)
    level64bck2tex=lovr.graphics.newTexture("externalassets/levels/level63/level63bck.jpg")
    level64bckmat = lovr.graphics.newMaterial(level64bcktex,1,1,1,1)
    level64bordertex=lovr.graphics.newTexture("externalassets/levels/level63/block.jpg")
    level64block = lovr.graphics.newMaterial(level64bordertex,1,1,1,1)
    
        elseif nLevel==65 then
  	
	level65bcktex=lovr.graphics.newTexture("externalassets/levels/level65/level65bck.jpg")
    level65bckmat = lovr.graphics.newMaterial(level65bcktex,1,1,1,1)
    level65bck2tex=lovr.graphics.newTexture("externalassets/levels/level65/level65bck.jpg")
    level65bckmat = lovr.graphics.newMaterial(level65bcktex,1,1,1,1)
    level65bordertex=lovr.graphics.newTexture("externalassets/levels/level65/block.jpg")
    level65block = lovr.graphics.newMaterial(level65bordertex,1,1,1,1)
    
        elseif nLevel==66 then
  	
	level66bcktex=lovr.graphics.newTexture("externalassets/levels/level66/level66bck.jpg")
    level66bckmat = lovr.graphics.newMaterial(level66bcktex,1,1,1,1)
    level66bck2tex=lovr.graphics.newTexture("externalassets/levels/level66/level66bck.jpg")
    level66bckmat = lovr.graphics.newMaterial(level66bcktex,1,1,1,1)
    level66bordertex=lovr.graphics.newTexture("externalassets/levels/level66/block.jpg")
    level66block = lovr.graphics.newMaterial(level66bordertex,1,1,1,1)
    
        elseif nLevel==67 then
  	
	level67bcktex=lovr.graphics.newTexture("externalassets/levels/level67/level67bck.jpg")
    level67bckmat = lovr.graphics.newMaterial(level67bcktex,1,1,1,1)
    level67bck2tex=lovr.graphics.newTexture("externalassets/levels/level67/level67bck.jpg")
    level67bckmat = lovr.graphics.newMaterial(level67bcktex,1,1,1,1)
    level67bordertex=lovr.graphics.newTexture("externalassets/levels/level67/block.jpg")
    level67block = lovr.graphics.newMaterial(level67bordertex,1,1,1,1)
    
        elseif nLevel==68 then
  	
	level68bcktex=lovr.graphics.newTexture("externalassets/levels/level68/level68bck.jpg")
    level68bckmat = lovr.graphics.newMaterial(level68bcktex,1,1,1,1)
    level68bck2tex=lovr.graphics.newTexture("externalassets/levels/level68/level68bck.jpg")
    level68bckmat = lovr.graphics.newMaterial(level68bcktex,1,1,1,1)
    level68bordertex=lovr.graphics.newTexture("externalassets/levels/level68/block.jpg")
    level68block = lovr.graphics.newMaterial(level68bordertex,1,1,1,1)
    
        elseif nLevel==69 then
  	
	level69bcktex=lovr.graphics.newTexture("externalassets/levels/level69/level69bck.jpg")
    level69bckmat = lovr.graphics.newMaterial(level69bcktex,1,1,1,1)
    level69bck2tex=lovr.graphics.newTexture("externalassets/levels/level69/level69bck.jpg")
    level69bckmat = lovr.graphics.newMaterial(level69bcktex,1,1,1,1)
    level69bordertex=lovr.graphics.newTexture("externalassets/levels/level69/block.jpg")
    level69block = lovr.graphics.newMaterial(level69bordertex,1,1,1,1)
    
        elseif nLevel==70 then
  	
	level70bcktex=lovr.graphics.newTexture("externalassets/levels/level69/level69bck.jpg")
    level70bckmat = lovr.graphics.newMaterial(level70bcktex,1,1,1,1)
    level70bck2tex=lovr.graphics.newTexture("externalassets/levels/level69/level69bck.jpg")
    level70bckmat = lovr.graphics.newMaterial(level70bcktex,1,1,1,1)
    level70bordertex=lovr.graphics.newTexture("externalassets/levels/level69/block.jpg")
    level70block = lovr.graphics.newMaterial(level70bordertex,1,1,1,1)
    
        elseif nLevel==71 then
  	
	level71bcktex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level71bckmat = lovr.graphics.newMaterial(level71bcktex,1,1,1,1)
    level71bck2tex=lovr.graphics.newTexture("externalassets/levels/level52/level52bck.jpg")
    level71bckmat = lovr.graphics.newMaterial(level71bcktex,1,1,1,1)
    level71bordertex=lovr.graphics.newTexture("externalassets/levels/level52/block.jpg")
    level71block = lovr.graphics.newMaterial(level71bordertex,1,1,1,1)
    
        elseif nLevel==72 then
  	
	level72bcktex=lovr.graphics.newTexture("externalassets/levels/level72/level72bck.jpg")
    level72bckmat = lovr.graphics.newMaterial(level72bcktex,1,1,1,1)
    level72bck2tex=lovr.graphics.newTexture("externalassets/levels/level72/level72bck.jpg")
    level72bckmat = lovr.graphics.newMaterial(level72bcktex,1,1,1,1)
    level72bordertex=lovr.graphics.newTexture("externalassets/levels/level3/level3bck2.jpg")
    level72block = lovr.graphics.newMaterial(level72bordertex,1,1,1,1)
    
        elseif nLevel==73 then
  	
	level73bcktex=lovr.graphics.newTexture("externalassets/levels/level72/level72bck.jpg")
    level73bckmat = lovr.graphics.newMaterial(level73bcktex,1,1,1,1)
    level73bck2tex=lovr.graphics.newTexture("externalassets/levels/level72/level72bck.jpg")
    level73bckmat = lovr.graphics.newMaterial(level73bcktex,1,1,1,1)
    level73bordertex=lovr.graphics.newTexture("externalassets/levels/level3/level3bck2.jpg")
    level73block = lovr.graphics.newMaterial(level73bordertex,1,1,1,1)
    
        elseif nLevel==74 then
  	
	level74bcktex=lovr.graphics.newTexture("externalassets/levels/level74/level74bck.jpg")
    level74bckmat = lovr.graphics.newMaterial(level74bcktex,1,1,1,1)
    level74bck2tex=lovr.graphics.newTexture("externalassets/levels/level74/level74bck.jpg")
    level74bckmat = lovr.graphics.newMaterial(level74bcktex,1,1,1,1)
    level74bordertex=lovr.graphics.newTexture("externalassets/levels/level74/block.jpg")
    level74block = lovr.graphics.newMaterial(level74bordertex,1,1,1,1)
    
        elseif nLevel==75 then
  	
	level75bcktex=lovr.graphics.newTexture("externalassets/levels/level61/level61bck.jpg")
    level75bckmat = lovr.graphics.newMaterial(level75bcktex,1,1,1,1)
    level75bck2tex=lovr.graphics.newTexture("externalassets/levels/level61/level61bck.jpg")
    level75bckmat = lovr.graphics.newMaterial(level75bcktex,1,1,1,1)
    level75bordertex=lovr.graphics.newTexture("externalassets/levels/level75/block.jpg")
    level75block = lovr.graphics.newMaterial(level75bordertex,1,1,1,1)
    
        elseif nLevel==76 then
  	
	level76bcktex=lovr.graphics.newTexture("externalassets/levels/level62/level62bck.jpg")
    level76bckmat = lovr.graphics.newMaterial(level76bcktex,1,1,1,1)
    level76bck2tex=lovr.graphics.newTexture("externalassets/levels/level62/level62bck.jpg")
    level76bckmat = lovr.graphics.newMaterial(level76bcktex,1,1,1,1)
    level76bordertex=lovr.graphics.newTexture("externalassets/levels/level76/block.jpg")
    level76block = lovr.graphics.newMaterial(level76bordertex,1,1,1,1)
    
        elseif nLevel==77 then
  	
	level77bcktex=lovr.graphics.newTexture("externalassets/levels/level77/level77bck.jpg")
    level77bckmat = lovr.graphics.newMaterial(level77bcktex,1,1,1,1)
    level77bck2tex=lovr.graphics.newTexture("externalassets/levels/level77/level77bck.jpg")
    level77bckmat = lovr.graphics.newMaterial(level77bcktex,1,1,1,1)
    level77bordertex=lovr.graphics.newTexture("externalassets/levels/level77/block.jpg")
    level77block = lovr.graphics.newMaterial(level77bordertex,1,1,1,1)
    
        elseif nLevel==78 then
  	
	level78bcktex=lovr.graphics.newTexture("externalassets/levels/level62/level62bck.jpg")
    level78bckmat = lovr.graphics.newMaterial(level78bcktex,1,1,1,1)
    level78bck2tex=lovr.graphics.newTexture("externalassets/levels/level62/level62bck.jpg")
    level78bckmat = lovr.graphics.newMaterial(level78bcktex,1,1,1,1)
    level78bordertex=lovr.graphics.newTexture("externalassets/levels/level78/block.jpg")
    level78block = lovr.graphics.newMaterial(level78bordertex,1,1,1,1)
    
        elseif nLevel==79 then
  	
	level79bcktex=lovr.graphics.newTexture("externalassets/levels/level79/level79bck.jpg")
    level79bckmat = lovr.graphics.newMaterial(level79bcktex,1,1,1,1)
    level79bck2tex=lovr.graphics.newTexture("externalassets/levels/level79/level79bck.jpg")
    level79bckmat = lovr.graphics.newMaterial(level79bcktex,1,1,1,1)
    level79bordertex=lovr.graphics.newTexture("externalassets/levels/level79/block.jpg")
    level79block = lovr.graphics.newMaterial(level79bordertex,1,1,1,1)
end
end
