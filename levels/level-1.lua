local level = {}

level.name = 'level-1'

-- first cell is map[1][1], top left corner
level.map = 
{	-- 0 is empty, 1 is full,
	-- 2 an higher are empty, but you can use it as placeholders
	-- 3 are borders
	-- 6 are exit areas
	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,1,0,0,0,1,1}, 
    {1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,1,0,0,0,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,0,3,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,3,3,3,3,0,0,0,0,0,3,3,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,3,3,3,1,1,1,1,1,1,1,1},
    {1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
    {1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
    {1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3},
    {1,1,1,1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1},
    {1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
}


level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 28,
		y = 17,
		w = 6,
		h = 4,
	}
}

level.blocks = 
{
	{name = 'chair1',
	heavy = false,
	x = 8,
	y = 18,
	form = 
		{
			{1,0,0,0},
			{1,0,0,0},
			{1,1,1,1},
			{1,0,0,1},
			{1,0,0,1},
		},
	},
	
	{name = 'table1',
	heavy = false,
	x = 13,
	y = 19,
	form = 
		{
		{1,1,1,1,1,1,1},
		{0,1,0,0,0,1,0},
		{0,1,0,0,0,1,0},
		{0,1,0,0,0,1,0},
		},
	},
	
	{name = 'chair2',
	heavy = false,
	x = 21,
	y = 18,
	form = 
		{
			{0,0,0,1},
			{0,0,0,1},
			{1,1,1,1},
			{1,0,0,1},
			{1,0,0,1},
		},
	},
	
	{name = 'steel-pipe-1x81',
	heavy = true,
	x = 16,
	y = 1,
	form = 
		{
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
		},
	},


	{name = 'cushion1',
	heavy = false,
	x = 9,
	y = 19,
	form = 
		{
		{1,1,1},
		},
	},
}

level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 13,
	y = 15,
	--x = 27, --exit area
	--y = 18,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 20,
	y = 15,
	--x = 27, --exit area
	--y = 19,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}


-- Define a function to prepare the map based on level data
function prepareMap(level)
    level.w = 0 -- map width in tiles (same as highest x)
    level.h = #level.map -- map height in tiles
    for y, xs in ipairs(level.map) do
        for x, value in ipairs(xs) do
            if value == 1 then
                -- now value is true
                level.map[y][x] = true
                if level.w < x then level.w = x end
            elseif value == 0 then
                -- now value is false
                level.map[y][x] = false
            elseif value == 2 then
                level.map[y][x] = "exit"
            elseif value == 3 then
                level.map[y][x] = "border"
            end
        end
    end

    for i, block in ipairs(level.blocks) do
        local w, h = 0, 0
        block.tiles = {}
        for y, xs in ipairs(block.form) do
            for x, value in ipairs(xs) do
                if value == 1 then
                    table.insert(block.tiles, x - 1) -- beware of -1
                    table.insert(block.tiles, y - 1)
                    if w < x then w = x end
                    if h < y then h = y end
                end
            end
        end
        block.w = w
        block.h = h
    end

    for i, agent in ipairs(level.agents) do
        local w, h = 0, 0
        agent.tiles = {}
        for y, xs in ipairs(agent.form) do
            for x, value in ipairs(xs) do
                if value == 1 then
                    table.insert(agent.tiles, x - 1) -- beware of -1
                    table.insert(agent.tiles, y - 1)
                    if w < x then w = x end
                    if h < y then h = y end
                end
            end
        end
        agent.w = w
        agent.h = h
        if not agent.direction then
            --setagentinitialdirection(agent)
        end
    end
end


prepareMap(level)

return level
