-- Define a function to prepare the map based on level data
function prepareMap(level)
    level.w = 0 -- map width in tiles (same as highest x)
    level.h = #level.map -- map height in tiles
    for y, xs in ipairs(level.map) do
        for x, value in ipairs(xs) do
            if value == 1 then
                -- now value is true
                level.map[y][x] = true
                if level.w < x then level.w = x end
            elseif value == 0 then
                -- now value is false
                level.map[y][x] = false
            elseif value == 2 then
                level.map[y][x] = "exit"
            elseif value == 3 then
                level.map[y][x] = "border"
            end
        end
    end

    for i, block in ipairs(level.blocks) do
        local w, h = 0, 0
        block.tiles = {}
        for y, xs in ipairs(block.form) do
            for x, value in ipairs(xs) do
                if value == 1 then
                    table.insert(block.tiles, x - 1) -- beware of -1
                    table.insert(block.tiles, y - 1)
                    if w < x then w = x end
                    if h < y then h = y end
                end
            end
        end
        block.w = w
        block.h = h
    end

    for i, agent in ipairs(level.agents) do
        local w, h = 0, 0
        agent.tiles = {}
        for y, xs in ipairs(agent.form) do
            for x, value in ipairs(xs) do
                if value == 1 then
                    table.insert(agent.tiles, x - 1) -- beware of -1
                    table.insert(agent.tiles, y - 1)
                    if w < x then w = x end
                    if h < y then h = y end
                end
            end
        end
        agent.w = w
        agent.h = h
        if not agent.direction then
            --setagentinitialdirection(agent)
        end
    end
end
