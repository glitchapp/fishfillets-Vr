warpshader3 = lovr.graphics.newShader([[
    #define P 0.0001 //derivative precision
    #define SIZE vec2(2, 20) //function size/precision
    #define WATER vec4(1) //water color

    float func(vec2 coords, float offset) {
      return (sin(coords.x - offset) * sin(coords.y + offset) + 1) / 20;
      //return (sin(coords.x - offset) + 1) / 2;
      //return cos(sqrt(pow(coords.x + offset, 2.0) + pow(coords.y + offset, 2.0)));
    }

    float xDerivative(vec2 coords, float offset) {
      return (func(coords + vec2(P, 0), offset) - func(coords - vec2(P, 0), offset))/((coords.x + P) - (coords.x - P));
    }

    float yDerivative(vec2 coords, float offset) {
      return (func(coords + vec2(0, P), offset) - func(coords - vec2(0, P), offset))/((coords.y + P) - (coords.y - P));
    }

    uniform float time = 0;

    vec4 effect(vec4 color, Image image, vec2 texCoords, vec2 scrCoords) {
      float waveHeight = func(texCoords * SIZE, time);

      float xDerivative = xDerivative(texCoords * SIZE, time);
      float yDerivative = yDerivative(texCoords * SIZE, time);

      vec2 off;
      off.x = xDerivative * waveHeight / SIZE.x;
      off.y = yDerivative * waveHeight / SIZE.y;

      //return vec4(xDerivative, yDerivative, 0, 1);
      //return vec4(waveHeight, 0, 0, 1);
      return Texel(image, texCoords + off) * WATER;
    }
    ]])
