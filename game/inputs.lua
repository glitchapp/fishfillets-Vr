function lovr.keypressed(key, scancode, isrepeat)

if key == 'w' or key == 'a' or key == 's' or key == 'd' then
    
    -- d means 1; a means -1; otherwise 0
    local dx = key == 'd' and 1 or key == 'a' and -1 or 0
    -- s means 1; w means -1; otherwise 0
    local dy = key == 's' and 1 or key == 'w' and -1 or 0
    pb:mainMoving(dx, dy)
  elseif key == 'right' or key == 'left' or key == 'up' or key == 'down' then
    local dx = key == 'right' and 1 or key == 'left' and -1 or 0
    local dy = key == 'down' and 1 or key == 'up' and -1 or 0
    pb:mainMoving(dx, dy)
  end
	
		--switch fishs
		if key == 'space' then 
			pb:switchAgent ()
	
		elseif key == "escape" then
			lovr.event.quit()
		end

		if key == 'space' and currentplayer=="1" then currentplayer="2"
	elseif key == 'space'  and currentplayer=="2" then currentplayer="1"
	end
	
				if key == 'f1' then
				loadbuttonpressed=true
				loadmygame()
			-- save game
			elseif key == 'f2' then
				savebuttonpressed=true
				savemygame()
				palette=1
			end
end

function lovr.controllerpressed(controller, button)
    if button == 'thumbstick' then
        local x, y = controller:getThumbstickAxes()
        -- Adjust sensitivity as needed
        local sensitivity = 0.1
        local dx = x * sensitivity
        local dy = y * sensitivity
        pb:mainMoving(dx, dy)
    elseif button == 'grip' then
        pb:switchAgent()
    elseif button == 'menu' then
        lovr.event.quit()
    end
end

