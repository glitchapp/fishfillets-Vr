
function levelload()



thumb=thumb1

spheretex= lovr.graphics.newTexture("/assets/levels/sphere.png")
spheremat=lovr.graphics.newMaterial(spheretex,1,1,1,1)

			if language=="es" then exittext="Finalizar" musictext="Musica" optionstext="Opciones" creditstext="Creditos" leditortext="Level editor" extrastext="Extras"
		elseif language=="de" then exittext="Beenden" musictext="Musik" optionstext="Einstellungen" creditstext="Credits" leditortext="Level editor" extrastext="Extras"
		elseif language=="en" then exittext="Exit"  musictext="Music" optionstext="Options" creditstext="Credits" leditortext="Level editor" extrastext="Extras" endtext="Ends"
		elseif language=="jp" then exittext="出口" musictext="音楽プレーヤー" optionstext="オプション" creditstext="修得" leditortext="レベルエディタ" extrastext="エキストラ" varelaroundfontmiddle = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
		elseif language=="chi" then exittext="退出" musictext="音乐播放器" optionstext="选项" creditstext="信用额度" leditortext="关卡编辑器" extrastext="额外" varelaroundfontmiddle = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
		elseif language=="thai" then exittext="ทางออก" musictext="เครื่องเล่นเพลง" optionstext="ตัวเลือก" creditstext="เครดิต" leditortext="แก้ไขระดับ" extrastext="ความพิเศษ" varelaroundfontmiddle = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
		elseif language=="tamil" then exittext="வெளியேற" musictext="இசைப்பான்" optionstext="ตัวเลือก" creditstext="வரவுகள்" leditortext="நிலை ஆசிரியர்" extrastext="கூடுதல்" varelaroundfontmiddle = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
		elseif language=="hindi" then exittext="बाहर निकलन" musictext="संगीत बजाने वाला" optionstext="विकल्प" creditstext="क्रेडिट" leditortext="स्तर संपादक" extrastext="एक्स्ट्रा कलाकार" varelaroundfontmiddle = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 34)
		end

--buttons

exitButton = {
text = exittext,
	x = 1000,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

musicButton = {
text = musictext,
	x = 100,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

creditsButton = {
text = creditstext,
	x = 100,
	y = 700, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

optionsButton = {
text = optionstext,
	x = 900,
	y = 700, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

leditorButton = {
text = leditortext,
	x = 1500,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

extrasButton = {
text = extrastext,
	x = 1500,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

endButton = {
text = endtext,
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = varelaroundfontmiddle,
}

--require("assets/text/levelmenu/level-en")
end

function levelbutton()
	
end

function levelupdate(dt)
	  
end

function drawfishhousesphere(x,y)
love.graphics.setFont(varelaroundfontmiddlesmall)
				love.graphics.print (zonetext[1],500, 850,0,1) 
				love.graphics.setFont(varelaroundfontmiddle)
				love.graphics.print ((y-1) .. '.' ..fishhouse0[y-1],500, 900,0,1)
				love.graphics.setFont(varelaroundfontmiddlesmall)
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,1)
end
	function drawshipwreckssphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[2],500, 850,0,1) 
	love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((x-5) .. '.' ..shipwrecks1[x-13], 500, 900,0,1)
	love.graphics.setFont(varelaroundfontmiddlesmall)
              love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
              love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..x-5, (x-1)*tileSize, (y-1)*tileSize,0,1)
	end
	
	function drawsilvershipsphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[3],500, 850,0,1) 
	love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((x+31) .. '.' ..silversship2[x-13], 500, 900,0,1)
		love.graphics.setFont(varelaroundfontmiddlesmall)
	     love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..x+31, (x-1)*tileSize, (y-1)*tileSize,0,1.2)
	 end
	function drawcitydeepsphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[4],500, 850,0,1) 
	love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((-x+32) .. '.' ..cityinthedeep3[(-x+13)], 500, 900,0,1)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	     love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..-x+32, (x-1)*tileSize, (y-1)*tileSize,0,1.1)
	 end
	 
	 function drawufosphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[5],500, 850,0,1) 
		love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((-x+64) .. '.' ..ufo4[(-x+13)], 500, 900,0,1)
		love.graphics.setFont(varelaroundfontmiddlesmall)
			  love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..-x+64, (x-1)*tileSize, (y-1)*tileSize,0,1)
	 end
	 
	 function drawcoralreefsphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[6],500, 850,0,1) 
	love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((x+16) .. '.' ..coralreef5[x-13], 500, 900,0,1)
		love.graphics.setFont(varelaroundfontmiddlesmall)
			  love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..x+16, (x-1)*tileSize, (y-1)*tileSize,0,1)
	 end
	 
	 	function drawtreasurecavesphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[7],500, 850,0,1) 
			love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((x+45) .. '.' ..treasurecave6[x-13], 500, 900,0,1)
		love.graphics.setFont(varelaroundfontmiddlesmall)
				love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..x+45, (x-1)*tileSize, (y-1)*tileSize,0,1)
	 end
	 
	 function drawdumpsphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[8],500, 850,0,1) 
		love.graphics.setFont(varelaroundfontmiddle)
	 love.graphics.print ((y+28) .. ' ' ..dump7[y-9],500,900,0,1)
	 love.graphics.setFont(varelaroundfontmiddlesmall)
	  		love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	  	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,600,-50+y*50,0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..y+28, (x-1)*tileSize, (y-1)*tileSize,0,1)
		 end
		 
		 function drawsecretcomputersphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[9],500, 850,0,1) 
	love.graphics.setFont(varelaroundfontmiddle)
	love.graphics.print ((x+51) .. '.' ..secretcomputer8[x-13], 500, 900,0,1)
	 love.graphics.setFont(varelaroundfontmiddlesmall)
		love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..x+51, (x-1)*tileSize, (y-1)*tileSize,0,1)
	end
	
	function drawsecretsphere(x,y)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.print (zonetext[10],500, 850,0,1) 
		love.graphics.setFont(varelaroundfontmiddle) 
	love.graphics.print ((-x+83) .. '.' ..secret9[(-x+13)], 500, 900,0,1)
	love.graphics.setFont(varelaroundfontmiddlesmall)
	love.graphics.setColor(0.5,0.5,0)
              love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
	love.graphics.setColor(1,1,0)			-- highlight level under cursor
            love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            love.graphics.setColor(1,1,1)
            love.graphics.print (' '..-x+83, (x-1)*tileSize, (y-1)*tileSize,0,1.2)
	end


--buttons

local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end

local function drawButton (button, hovered,text)
	
	
	--love.graphics.setFont( button.font )
	  love.graphics.setFont(varelaroundfontmiddle)
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(text,button.x,button.y,button.r,button.sx)
end



function leveldraw()
local i=1

	--local mx, my = 1,1
	local mx, my = lovr.mouse.getPosition()
	--local mx, my = lovr.mouse.getPosition()
	tileSize = 51
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1
	--print ("mx" .. mx)
	--print ("my" .. my)
		 
	lovr.graphics.newFont( varelaroundfontmiddlesmall )
        for i=1, 8, 1 do		-- 0. Fish house 				(8 levels)
			lovr.graphics.setColor(0,1,0)
            --lovr.graphics.plane("fill",600,i*50,0,10,10)
            lovr.graphics.sphere(spheremat,0,(i*2)-10,-10,1)
            lovr.graphics.setColor(0,0,0)
            lovr.graphics.print(i,0,(-i*2)+8,-9,1)
            lovr.graphics.setColor(1,1,1)
             --dream:draw(yourObject, (x - 25) , -y+13, -14)
                --lovr.graphics.plane('fill',(x - 1) * cellWidth, (y - 1) * cellHeight,0,cellSize,cellSize)
    
     --lovr.graphics.cube ('line', 100,100,0, 100)
        --lovr.graphics.sphere (100,100,0, 100)
        end
     print(x)
        if x==7 or x==8 then lovr.graphics.setColor(1,1,0)		--highlight level under cursor
            lovr.graphics.sphere(spheremat,0,50-my/6,-10,1)
            lovr.graphics.setColor(1,1,1)
			end
     
        for i=1, 11, 1 do		-- 1. Ship Wrecks (9 - 19) 		11 levels
			if i<12 then lovr.graphics.setColor(0,1,0) end
            lovr.graphics.sphere(spheremat,i*2,0,-10,1)
            lovr.graphics.setColor(0,0,0)
            lovr.graphics.print(i+8,i*2,0,-9,1)
            lovr.graphics.setColor(1,1,1)
                  
        end
--[[           
         for i=1, 7, 1 do		-- 2. Silver's ship (45-51)
			if i<11 then lovr.graphics.setColor(0,1,0) end
            lovr.graphics.sphere(600+i*50,500,0,1)
            lovr.graphics.setColor(0,0,0)
            lovr.graphics.print(i+44,615+i*50,510,0,1)
            lovr.graphics.setColor(1,1,1)
        
        end
        
        for i=1, 10, 1 do		-- 3. City in the deep (20-29)	 10 levels
        	lovr.graphics.setColor(0,1,0)
            lovr.graphics.sphere(610-i*50,250,0,0.1)
            lovr.graphics.setColor(0,0,0)
            lovr.graphics.print(i+19,620-i*50,260,0,1)
            lovr.graphics.setColor(1,1,1)
           
        end
     --[[   
        for i=1, 7, 1 do		-- 4. UFO (52-58)	 6 levels
        	love.graphics.setColor(0,1,0)
            love.graphics.draw(sphere,610-i*50,400,0,0.1)
            love.graphics.setColor(0,0,0)
            love.graphics.print(i+51,620-i*50,410,0,1)
            love.graphics.setColor(1,1,1)
            
        
        end
        
        for i=1, 8, 1 do		--5 Coral Reef	(30 - 37)		7 levels
			love.graphics.setColor(0,1,0)
            love.graphics.draw(sphere,600+i*50,350,0,0.1)
            love.graphics.setColor(0,0,0)
             love.graphics.print(i+29,610+i*50,360,0,1)
             love.graphics.setColor(1,1,1)
                
        end
        
          for i=1, 6, 1 do		-- 6. Treasure cave	 (59-64)
			love.graphics.setColor(0,1,0)
            love.graphics.draw(sphere,600+i*50,600,0,0.1)
            love.graphics.setColor(0,0,0)
            love.graphics.print(i+58,615+i*50,610,0,1)
            love.graphics.setColor(1,1,1)
            
        --shadows
        newBody = Body:new(newLightWorld)
		newBody:SetPosition(320+i*2*1, 310)
		CircleShadow:new(newBody, 300+i*50*1, 310, 16)  
        end
        
        				
        for i=1, 7, 1 do		-- 7. Dump (38-44) 				(7 levels)
            love.graphics.setColor(0,1,0)
            love.graphics.draw(sphere,600,400+i*50,0,0.1)
            love.graphics.setColor(0,0,0)
            love.graphics.print(i+37,610,400+10+i*50,0,1)
            love.graphics.setColor(1,1,1)
        
        end
        
        for i=1, 6, 1 do		--8 Secret computer
            love.graphics.setColor(0,1,0)
            love.graphics.draw(sphere,600+i*50,300,0,0.1)
            love.graphics.setColor(0,0,0)
             love.graphics.print(i+64,610+i*50,310,0,1)
             love.graphics.setColor(1,1,1)
             
        end
        
        
        for i=1, 9, 1 do		-- 9 Secret. 71-79
        	love.graphics.setColor(0,1,0)
            love.graphics.draw(sphere,610-i*50,550,0,0.1)
            love.graphics.setColor(0,0,0)
            love.graphics.print(i+70,620-i*50,560,0,1)
            love.graphics.setColor(1,1,1)
        
        end
        

    
	--love.graphics.draw(levelmap,500,300,0,1)
	--level 1-8			0. fish house
	if x>12 and x<14 and y>1 and y<10 then 
	    	   
        if y==2 and levelac1.open==true or y==3 and levelac2.open==true or y==4 and levelac3.open==true or y==5 and levelac4.open==true or y==6 and levelac5.open==true or y==7 and levelac6.open==true or y==8 and levelac7.open==true or y==9 and levelac8.open==true then 
        drawfishhousesphere(x,y) end
		        
      love.graphics.setFont(varelaroundfontmiddlesmall)
            love.graphics.print (' '..y-1, (x-1)*tileSize, (y-1)*tileSize,0,1)
            
            
		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
		love.timer.sleep( 0.5 )
		if y==2 and levelac1.open==true or y==3 and levelac2.open==true or y==4 and levelac3.open==true or y==5 and levelac4.open==true or y==6 and levelac5.open==true or y==7 and levelac6.open==true or y==8 and levelac7.open==true or y==9 and levelac8.open==true then
		nLevel=y-1 changelevel() gamestatus="game" end
		timer=0
		stepdone=0

		end
	end
	               --shadows
    if x>10 and x<16 and y>0 and y<12 then
        for i=1, 8, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, i*2*1)
			CircleShadow:new(newBody, 0, 20+i*50*1, 16)
		end
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
		
	--level 9-19				--1. Ship Wrecks
	if y==5 and x>13 and x<25 then
	    if x==13 and levelac9.open==true or x==14 and levelac10.open==true or x==15 and levelac10.open==true or x==16 and levelac11.open==true or x==17 and levelac12.open==true or x==18 and levelac13.open==true or x==19 and levelac14.open==true or x==20 and levelac15.open==true or x==21 and levelac16.open==true or x==22 and levelac17.open==true or x==23 and levelac18.open==true or x==24 and levelac19.open==true then
	    drawshipwreckssphere(x,y) end
		

		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
		love.timer.sleep( 0.5 )
			if x==13 and levelac9.open==true or x==14 and levelac10.open==true or x==15 and levelac10.open==true or x==16 and levelac11.open==true or x==17 and levelac12.open==true or x==18 and levelac13.open==true or x==19 and levelac14.open==true or x==20 and levelac15.open==true or x==21 and levelac16.open==true or x==22 and levelac17.open==true or x==23 and levelac18.open==true or x==24 and levelac19.open==true then
				nLevel=x-5
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
			end
		end
	end
	
				--shadows 	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 then
			for i=1, 11, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 110)
				CircleShadow:new(newBody, 300+i*50*1, 110, 16)
			end
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
			
		end
	
	--level 45-51	 			2 Silver's ship
	
	if y==11 and x>13 and x<21 then
	
	if x==14 and levelac45.open==true or x==15 and levelac46.open==true or x==16 and levelac47.open==true or x==17 and levelac48.open==true or x==18 and levelac49.open==true or x==19 and levelac50.open==true or x==20 and levelac51.open==true then
	drawsilvershipsphere(x,y)
	end

		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
				love.timer.sleep( 0.5 )
				if x==14 and levelac45.open==true or x==15 and levelac46.open==true or x==16 and levelac47.open==true or x==17 and levelac48.open==true or x==18 and levelac49.open==true or x==19 and levelac50.open==true or x==20 and levelac51.open==true then
				nLevel=x+31
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
				end
		end
		end
		     --shadows --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 then 
			for i=1, 7, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 260)
				CircleShadow:new(newBody, 300+i*50*1, 260, 16)
			end            
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end

	
	
	--level 20-29			--3. City in the deep
	if y==6 and x>2 and x<13 then
	if x==12 and levelac20.open==true or x==11 and levelac21.open==true or x==10 and levelac22.open==true or x==9 and levelac23.open==true or x==8 and levelac24.open==true or x==7 and levelac25.open==true or x==6 and levelac26.open==true or x==5 and levelac27.open==true or x==4 and levelac28.open==true or x==3 and levelac29.open==true then
	drawcitydeepsphere(x,y)
	end

		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
			love.timer.sleep( 0.5 )
			if x==12 and levelac20.open==true or x==11 and levelac21.open==true or x==10 and levelac22.open==true or x==9 and levelac23.open==true or x==8 and levelac24.open==true or x==7 and levelac25.open==true or x==6 and levelac26.open==true or x==5 and levelac27.open==true or x==4 and levelac28.open==true or x==3 and levelac29.open==true then
				nLevel=-x+32
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
			end
		end
	end
	
        --shadows 	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 then 
			for i=1, 10, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(315-i*0.5, 140)
				CircleShadow:new(newBody, 315-i*50*1, 140, 16)
			end
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
		--level 52-58			4.UFO
	if y==9 and x>5 and x<13 then
	if x==12 and levelac52.open==true or x==11 and levelac53.open==true or x==10 and levelac54.open==true or x==9 and levelac55.open==true or x==8 and levelac56.open==true or x==7 and levelac57.open==true or x==6 and levelac58.open==true then
	drawufosphere(x,y)
	end
	
	 	 
		if love.mouse.isDown(1) then
				nLevel=-x+64
				if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
			love.timer.sleep( 0.5 )
			if x==12 and levelac52.open==true or x==11 and levelac53.open==true or x==10 and levelac54.open==true or x==9 and levelac55.open==true or x==8 and levelac56.open==true or x==7 and levelac57.open==true or x==6 and levelac58.open==true then
			changelevel()
			gamestatus="game"
			timer=0
			stepdone=0
			end
		end
	end
	
	--shadows 		--level 52-58
	if y>8 and y<10 and x>4 and x<14 then
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(315-i*0.5, 210)
			CircleShadow:new(newBody, 315-i*50*1, 210, 16)
		end   
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
	--level 30-37			5. Coral reef
	if y==8 and x>13 and x<22 then
	if x==14 and levelac30.open==true or x==15 and levelac31.open==true or x==16 and levelac32.open==true or x==17 and levelac33.open==true or x==18 and levelac34.open==true or x==19 and levelac35.open==true or x==20 and levelac36.open==true or x==21 and levelac37.open==true then
	drawcoralreefsphere(x,y)
	
	end
	   	        
		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
			love.timer.sleep( 0.5 )
				if x==14 and levelac30.open==true or x==15 and levelac31.open==true or x==16 and levelac32.open==true or x==17 and levelac33.open==true or x==18 and levelac34.open==true or x==19 and levelac35.open==true or x==20 and levelac36.open==true or x==21 and levelac37.open==true then
				nLevel=x+16
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
				end
		end
	end
	
	    --shadows --level 30-37
	    if y>7 and y<9 and x>12 and x<23 then 
			for i=1, 8, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	
	--level 59 64		6. Treasure cave 
	
		if y==13 and x>13 and x<21 then
		if x==14 and levelac59.open==true or x==15 and levelac60.open==true or x==16 and levelac61.open==true or x==17 and levelac62.open==true or x==18 and levelac63.open==true or x==19 and levelac64.open==true then
		drawtreasurecavesphere(x,y)
		end
	 	 
		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
			love.timer.sleep( 0.5 )
			if x==14 and levelac59.open==true or x==15 and levelac60.open==true or x==16 and levelac61.open==true or x==17 and levelac62.open==true or x==18 and levelac63.open==true or x==19 and levelac64.open==true then
				nLevel=x+45
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
			end
		end
	end
	
	if android==false then
	
	    --shadows --level 59 64
	    if y==13 and x>13 and x<21 then
			for i=1, 4, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
        
        end
	
	
	--level 38-44			7. Dump
	if x==13 and y>9 and y<17 then
	if y==10 and levelac38.open==true or y==11 and levelac39.open==true or y==12 and levelac40.open==true or y==13 and levelac41.open==true or y==14 and levelac42.open==true or y==15 and levelac43.open==true or y==16 and levelac44.open==true then
	drawdumpsphere(x,y)
	end
		 
		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
		love.timer.sleep( 0.5 )
			if y==10 and levelac38.open==true or y==11 and levelac39.open==true or y==12 and levelac40.open==true or y==13 and levelac41.open==true or y==14 and levelac42.open==true or y==15 and levelac43.open==true or y==16 and levelac44.open==true then
            nLevel=y+27
      		changelevel()
			gamestatus="game"
			timer=0
			stepdone=0
			end
        end
     end
     
      --shadows --level 38-44
      if x>12 and x<14 and y>8 and y<18 then 
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, 200+i*2*1)
			CircleShadow:new(newBody, 0, 200+i*50*1, 16)
		end
       else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
       end
     
     	--level 65-	70					--8. Secret computer
	if y==7 and x>13 and x<20 then
	if x==14 and levelac65.open==true or x==15 and levelac66.open==true or x==16 and levelac67.open==true or x==17 and levelac68.open==true or x==18 and levelac69.open==true or x==19 and levelac70.open==true then
	drawsecretcomputersphere(x,y)
	 end
	 
		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
		love.timer.sleep( 0.5 )
				if x==14 and levelac65.open==true or x==15 and levelac66.open==true or x==16 and levelac67.open==true or x==17 and levelac68.open==true or x==18 and levelac69.open==true or x==19 and levelac70.open==true then
				nLevel=x+51
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
				end
		end
	end
	
		 --shadows    --level 65-	70
		if y>6 and y<8 and x>12 and x<21 then
			for i=1, 6, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 160)
				CircleShadow:new(newBody, 300+i*50*1, 160, 16)
			end
        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
                
        end
	
			--level 71-79			9. Secret
	if y==12 and x>3 and x<13 then
	if x==12 and levelac71.open==true or x==11 and levelac72.open==true or x==10 and levelac73.open==true or x==9 and levelac74.open==true or x==8 and levelac75.open==true or x==7 and levelac76.open==true or x==6 and levelac77.open==true or x==5 and levelac78.open==true or x==4 and levelac79.open==true then
	drawsecretsphere(x,y)
	end
     
		if love.mouse.isDown(1) then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
			love.timer.sleep( 0.5 )
				if x==12 and levelac71.open==true or x==11 and levelac72.open==true or x==10 and levelac73.open==true or x==9 and levelac74.open==true or x==8 and levelac75.open==true or x==7 and levelac76.open==true or x==6 and levelac77.open==true or x==5 and levelac78.open==true or x==4 and levelac79.open==true then
				nLevel=-x+82
				changelevel()
				gamestatus="game"
				timer=0
				stepdone=0
				end
		end
	end
	
			    
        --shadows --level 71-79
        if y>11 and y<13 and x>2 and x<14 then 
			for i=1, 9, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(315-i*0.5, 290)
				CircleShadow:new(newBody, 315-i*50*1, 290, 16)
			end
        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
     
	
			--level 1-8	0. fish house 				1. Ship Wrecks level 9-19  		2 Silver's ship					3. City in the deep level 20-29		4.UFO level 52-58    		5. Coral reef level 30-37	6. Treasure cave  level 59 64   	7. Dump deep level 38-44  	8. Secret computer     9. Secret level 71-79
	
	if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)

	
	else newLight:SetColor(0, 255, 0, 100)
	end


	--exit
	local hovered = isButtonHovered (exitButton)
	drawButton (exitButton, hovered,exittext)
	
	if hovered and love.mouse.isDown(1) then 
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
				love.timer.sleep( 0.5 )
				love.event.quit()
			end
	end
	
		--Music player
		
			local hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)
	
	if hovered and love.mouse.isDown(1) then 
		if soundon==true then gui21:play() end
		if love.mouse.isDown(1) then 
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
				  music:stop()
				love.timer.sleep( 0.5 )
				gamestatus="music"
			end
		end
	end
	
			--Options
		
		local hovered = isButtonHovered (optionsButton)
	drawButton (optionsButton, hovered,optionstext)

	if hovered and love.mouse.isDown(1) then 
		if soundon==true then gui21:play() end
			if love.mouse.isDown(1) then 
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static")
			  music:stop()
				gamestatus="options" 
			end
		end
	end
	
	
			--Credits

		local hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditstext)

	if hovered and love.mouse.isDown(1) then 
		if soundon==true then gui21:play() end
			if love.mouse.isDown(1) then
				if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") 
					--play music
				love.timer.sleep( 0.2 )
					love.audio.stop() music:stop()
					if musicison==true then Vaporware = love.audio.newSource("externalassets/music/pixelsphere/003_Vaporwareloop.ogg","stream" ) end
					if musicison==true then love.timer.sleep( 0.5 ) love.audio.play( Vaporware ) end
				gamestatus="credits" 
			end
		end
	end
	
if leveleditorunlocked==true then	
	-- Level editor
		
		local hovered = isButtonHovered (leditorButton)
	drawButton (leditorButton, hovered,leditortext)

	if hovered and love.mouse.isDown(1) then 
		if soundon==true then gui21:play() end
			if love.mouse.isDown(1) then 
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static")
			  music:stop()
				gamestatus="leditor"
			end
		end
	end	
end
if extrasunlocked==true then	
	-- extras
		
		local hovered = isButtonHovered (extrasButton)
	drawButton (extrasButton, hovered,extrastext)

	if hovered and love.mouse.isDown(1) then 
		if soundon==true then gui21:play() end
			if love.mouse.isDown(1) then 
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static")
			  music:stop()
				gamestatus="extras" 
			end
		end
	end	
end
	if endmenuunlocked==true then
		-- ends
			local hovered = isButtonHovered (endButton)
	drawButton (endButton, hovered,endtext)

	if hovered and love.mouse.isDown(1) then 
		if soundon==true then gui21:play() end
			if love.mouse.isDown(1) then 
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static")
			  music:stop()
			  require ('game/ends/endmenu')		-- Load ends menu
			  endmenuload()
				gamestatus="endmenu"
			end
		end
	end	
	
	end
	--love.graphics.setFont(font1)
	--love.graphics.print (x..' '..y, (x-1)*tileSize, (y-2)*tileSize)
	--]]
	
end
