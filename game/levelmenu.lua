
function levelload()
	thumb=thumb1

subbeat=0

spheretex= lovr.graphics.newTexture("/assets/levels/sphere.png")
spheremat=lovr.graphics.newMaterial(spheretex,1,1,1,1)

	if language=="es" then exittext="Finalizar" musictext="Musica" optionstext="Opciones" creditstext="Creditos" leditortext="Level editor" extrastext="Extras"
	elseif language=="de" then exittext="Beenden" musictext="Musik" optionstext="Einstellungen" creditstext="Credits" leditortext="Level editor" extrastext="Extras"
	elseif language=="en" then exittext="Exit"  musictext="Music" optionstext="Options" creditstext="Credits" leditortext="Level editor" extrastext="Extras" endtext="Ends" lovr.graphics.newFont( varelaroundfontmiddlesmall )
	elseif language=="jp" then exittext="出口" musictext="音楽プレーヤー" optionstext="オプション" creditstext="修得" leditortext="レベルエディタ" extrastext="エキストラ" poorfishmiddle = lovr.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
	elseif language=="chi" then exittext="退出" musictext="音乐播放器" optionstext="选项" creditstext="信用额度" leditortext="关卡编辑器" extrastext="额外" poorfishmiddle = lovr.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
	elseif language=="thai" then exittext="ทางออก" musictext="เครื่องเล่นเพลง" optionstext="ตัวเลือก" creditstext="เครดิต" leditortext="แก้ไขระดับ" extrastext="ความพิเศษ" poorfishmiddle = lovr.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
	elseif language=="tamil" then exittext="வெளியேற" musictext="இசைப்பான்" optionstext="ตัวเลือก" creditstext="வரவுகள்" leditortext="நிலை ஆசிரியர்" extrastext="கூடுதல்" poorfishmiddle = lovr.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
	elseif language=="hindi" then exittext="बाहर निकलन" musictext="संगीत बजाने वाला" optionstext="विकल्प" creditstext="क्रेडिट" leditortext="स्तर संपादक" extrastext="एक्स्ट्रा कलाकार" poorfishmiddle = lovr.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 34)
	end

--buttons

	exitButton = {
		text = exittext,
		x = 1000,
		y = 10,
		z = -10,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = font3,
	}

	musicButton = {
		text = musictext,
		x = 100,
		y = 10, 
		z = -10,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = font3,
	}

	creditsButton = {
		text = creditstext,
		x = 100,
		y = 700,
		z = -10,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = font3,
	}

	optionsButton = {
		text = optionstext,
		x = 		900,
		y = 700,
		z = -10,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = font3,
	}

	leditorButton = {
	text = leditortext,
	x = 1500,
	y = 10,
	z = -10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	extrasButton = {
	text = extrastext,
	x = 1500,
	y = 100,
	z = -10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	endButton = {
	text = endtext,
	x = 1500,
	y = 200,
	z = -10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	require("assets/text/levelmenu/level-en")
	
end




--buttons

local function isButtonHovered (button)
	local font = button.font or lovr.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y, z = button.x, button.y, button.z
	local w, h = width*sx, height*sy
	local mx, my = lovr.mouse.getPosition()
	if mx >= x and mx <= x+w
	and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end

local function drawButton (button, hovered,text)

	lovr.graphics.setFont( button.font )

	if hovered then
		lovr.graphics.setColor(button.hoveredColor)
		lovr.graphics.rectangle ('line', button.x, button.y,button.z, button.w, button.h)
	else
		lovr.graphics.setColor(button.color)
	end
	lovr.graphics.print(text,button.x,button.y,button.r,button.sx)
end


local function drawTile (x, y, tileSize)
	local color = {0.2, 0.2, 0.2}
	if (x+y)%2 == 0 then
		color = {0.3, 0.3, 0.3}
	end
	lovr.graphics.setColor (color)
	lovr.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, (z-1)*tileSize,tileSize, tileSize)
end

local function drawCenteredText (rectX, rectY,rectZ, rectWidth, rectHeight, text)
	local font       = lovr.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	lovr.graphics.print(text, 
		rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, 
		math.floor(textWidth/2)+0.5, 
		math.floor(textHeight/2)+0.5)
end


local function drawCenteredSprite (x, y, tileSize, sprite)
	--x and y it tiles, [1,1] is top left
	--local w, h = sprite:getDimensions()
	w,h,z=300,30,30
	local scale = tileSize/math.max (w, h)
	
	
	
	lovr.graphics.sphere (spheremat, x-10,-y+10,-20,0.8)
	lovr.graphics.setColor(0,0,0,1)	-- Setting the drawing color to white
	lovr.graphics.print(level,(x-10)*1,(-y+10)*1,-19,1)
	lovr.graphics.setColor(1,1,1,1)	-- Setting the drawing color to white
	
end


local spheres = {
-- 0. Fish house (8 levels)
	{level= 1, x=13.9, y=5.6},
	{level= 2, x=14.2, y=6.9},
	{level= 3, x=13.8, y=8.4},
	{level= 4, x=13, y=9.9},
	{level= 5, x=12.4, y=11},
	{level= 6, x=12.2, y=12.4},
	{level= 7, x=12.7, y=13.6},
	{level= 8, x=13.6, y=14.6},

-- 1. Ship Wrecks (9 - 19) (11 levels)
	{level= 9, x=14.7, y=10},
	{level=10, x=16.4, y=9.8},
	{level=11, x=18.1, y=9.3},
	{level=12, x=19.5, y=8.4},
	{level=13, x=20.6, y=7.1},
	{level=14, x=21,   y=5.5},
	{level=15, x=20.4, y=4},
	{level=16, x=19.1, y=3},
	{level=17, x=17.3, y=3.1},
	{level=18, x=16,  y=4.2},
	{level=19, x=16.8,  y=5.7},

-- 3. City in the deep (20-29)	 10 levels
	{level=20, x=10.8, y=10.3},
	{level=21, x=9.6, y=9.6},
	{level=22, x=8.5, y=8.5},
	{level=23, x=7.6, y=7.2},
	{level=24, x=7.3, y=5.7},
	{level=25, x= 7.8, y=3.9},
	{level=26, x= 9.2, y=2.7},
	{level=27, x= 10.7, y=2.4},
	{level=28, x= 11.6, y=3.7},
	{level=29, x= 11, y=5.3},
	
--5 Coral Reef	(30 - 37)		7 levels

	{level=30, x=14.2, y=12.8},
	{level=31, x=15.8, y=12.8},
	{level=32, x=17.2, y=13.6},
	{level=33, x=18, y=15},
	{level=34, x=17.7, y=16.5},
	{level=35, x=16.5, y=17.5},
	{level=36, x=15.1, y=17.5},
	{level=37, x=14.5, y=16.1},

-- 7. Dump (38-44) 				(7 levels)
		
	{level=38, x=12.6, y=15.9},
	{level=39, x=11.2, y= 17.1},
	{level=40, x=9.6, y= 17.8},
	{level=41, x=7.6, y= 17.8},
	{level=42, x=6.2, y= 16.5},
	{level=43, x=6.9, y= 14.7},
	{level=44, x=8.5, y= 15.2},

-- 2. Silver's ship (45-51) (7 levels)
	
	{level=45, x=19.9, y=10},
	{level=46, x=20.8, y=11.3},
	{level=47, x=22.2, y=11.9},
	{level=48, x=23.8, y=11.1},
	{level=49, x=24.3, y=9.3},
	{level=50, x=23.4, y=7.8},
	{level=51, x=21.7, y=8.9},

-- 4. UFO (52-58)	 (7 levels) (not 6)
	
	{level=52, x=7.4, y= 9.8},
	{level=53, x=6, y= 10.4},
	{level=54, x=4.4, y= 10.4},
	{level=55, x= 3.4, y= 9.4},
	{level=56, x= 3.7, y= 7.5},
	{level=57, x= 5.2, y= 7.2},
	{level=58, x= 5.5, y= 8.5},
-- 6. Treasure cave	 (59-64) (6 levels)

	
	{level=59, x=11.1, y=15.1},
	{level=60, x=10, y=13.7},
	{level=61, x=8.8, y=12.6},
	{level=62, x=7.3, y=12.1},
	{level=63, x=5.9, y=12.5},
	{level=64, x=4.6, y=13.6},


-- 8. Secret computer (65-70) (6 levels)

	{level=65, x=17.5, y=12.2},
	{level=66, x=18.9, y=12.7},
	{level=67, x=20, y=13.8},
	{level=68, x=21.1, y=14.7},
	{level=69, x=22.6, y=14.4},
	{level=70, x=23.7, y=13.7},

-- 9 Secret.			 (71-79) (9 levels)
	{level=71, x= 9, y=20},
	{level=72, x= 8, y=20},
	{level=73, x= 7, y=20},
	{level=74, x= 6, y=20},
	{level=75, x= 5, y=20},
	{level=76, x= 4, y=20},
	{level=77, x= 3, y=20},
	{level=78, x= 2, y=20},
	{level=79, x= 1, y=20},

}

-- function draw selection circle synced to the music

	
function drawfishhousesphere(x,y)
				if y>1 and y<10 and x==13 then
					lovr.graphics.setColor(0.5,0.5,0)
					lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
					lovr.graphics.setColor(1,1,1)
					lovr.graphics.print (zonetext[1],500, 850,0,1) 
					lovr.graphics.print ((y-1) .. '.' ..fishhouse0[y-1],500, 900,0,1) 
					--lovr.graphics.print (' '..y-1, (x-1)*tileSize, (y-1)*tileSize,0,1)
				end
end
	function drawshipwreckssphere(x,y)
		if x>13 and x<25 and y==5 then 
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
				--lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
        		lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[2],500, 850,0,1) 
				lovr.graphics.print ((x-5) .. '.' ..shipwrecks1[x-13], 500, 900,0,1)
                --lovr.graphics.print (' '..x-5, (x-1)*tileSize, (y-1)*tileSize,0,1)
			 end
	end
	
	function drawsilvershipsphere(x,y)
		if x>13 and x<21 and y==11 then 
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
				--lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            	lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[3],500, 850,0,1) 
				lovr.graphics.print ((x+31) .. '.' ..silversship2[x-13], 500, 900,0,1) 
				--lovr.graphics.print (' '..x+31, (x-1)*tileSize, (y-1)*tileSize,0,1.2)
            end
	 end
	

	
	function drawcitydeepsphere(x,y)
		if x>2 and x<13 and y==6 then
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
				--lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
               	lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[4],500, 850,0,1) 
				lovr.graphics.print ((-x+32) .. '.' ..cityinthedeep3[(-x+13)], 500, 900,0,1)
				--lovr.graphics.print (' '..-x+32, (x-1)*tileSize, (y-1)*tileSize,0,1.1)
             end
	 end

	 function drawufosphere(x,y)
		if x>5 and x<13 and y==9 then
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
				--lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
				lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[5],500, 850,0,1) 
				lovr.graphics.print ((-x+64) .. '.' ..ufo4[(-x+13)], 500, 900,0,1) 
				--lovr.graphics.print (' '..-x+64, (x-1)*tileSize, (y-1)*tileSize,0,1)
        end
	 end
	 
	 function drawcoralreefsphere(x,y)
		if x>13 and x<22 and y==8 then
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
			-- lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            	lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[6],500, 850,0,1) 
				lovr.graphics.print ((x+16) .. '.' ..coralreef5[x-13], 500, 900,0,1) 
				--lovr.graphics.print (' '..x+16, (x-1)*tileSize, (y-1)*tileSize,0,1)
            end
	 end

	 
	 	function drawtreasurecavesphere(x,y)
			if x>13 and x<20 and y==13 then
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
				-- lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            	lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[7],500, 850,0,1) 
				lovr.graphics.print ((x+45) .. '.' ..treasurecave6[x-13], 500, 900,0,1) 
				--lovr.graphics.print (' '..x+45, (x-1)*tileSize, (y-1)*tileSize,0,1)
            end
	 end
	
	 function drawdumpsphere(x,y)
		if y>9 and y<17 and x==13 then
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
			-- lovr.graphics.draw(sphere,600,-50+y*50,0,0.1)
            	lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[8],500, 850,0,1) 
				lovr.graphics.print ((y+28) .. ' ' ..dump7[y-9],500,900,0,1) 
				--lovr.graphics.print (' '..y+28, (x-1)*tileSize, (y-1)*tileSize,0,1)
            end
		 end
	
		 function drawsecretcomputersphere(x,y)
			if x>13 and x<20 and y==7 then
				lovr.graphics.setColor(0.5,0.5,0)
				lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
				--lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
				lovr.graphics.setColor(1,1,1)
				lovr.graphics.print (zonetext[9],500, 850,0,1) 
				lovr.graphics.print ((x+51) .. '.' ..secretcomputer8[x-13], 500, 900,0,1)
				--lovr.graphics.print (' '..x+51, (x-1)*tileSize, (y-1)*tileSize,0,1)
			end
	end

	function drawsecretsphere(x,y)
		if x>3 and x<13 and y==12 then
			lovr.graphics.setColor(0.5,0.5,0)
			lovr.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
			lovr.graphics.setColor(1,1,0)			-- highlight level under cursor
            --lovr.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
        	lovr.graphics.setColor(1,1,1)
			lovr.graphics.print (zonetext[10],500, 850,0,1) 
			lovr.graphics.print ((-x+83) .. '.' ..secret9[(-x+13)], 500, 900,0,1) 
            --lovr.graphics.print (' '..-x+83, (x-1)*tileSize, (y-1)*tileSize,0,1.2)
         end
	end

function leveldraw()
	local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16

	local width, height = lovr.graphics.getDimensions()
	
	-- to make constant tilesize:
	tileSize = 50

	-- to make the spheres adaptive to window size:
	tileSize = math.min (width/tileWidth, height/tileHeight)
	tileSize = math.min (50, tileSize)




	local mx, my = lovr.mouse.getPosition()

	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1

	lovr.graphics.setFont(font3)

	for i, spherelevel in ipairs (spheres) do
		level = spherelevel.level
		local x, y ,z = spherelevel.x, spherelevel.y, spherelevel.z

		lovr.graphics.setColor(0,1,0)
		drawCenteredSprite (x, y, tileSize, sphere)

		lovr.graphics.setColor(0,0,0)
		drawCenteredText ((x-1)*tileSize, (y-1)*tileSize, 0, tileSize, tileSize, level)
	end


		
		
	local sphereHovered

	for i, spherelevel in ipairs (spheres) do
		local level = spherelevel.level
		local x1, y1, z1 = spherelevel.x, spherelevel.y, spherelevel.z
		if x1 == x and y1 == y then
			sphereHovered = spherelevel
			break
		end
	end

	if sphereHovered then
		local level = sphereHovered.level
		local x1, y1 = sphereHovered.x, sphereHovered.y
		lovr.graphics.setColor(1,1,0)
		--drawCenteredSprite (x1, y1, tileSize, sphere)

		lovr.graphics.setColor(1,1,1)
		drawCenteredText ((x1-1)*tileSize, (y1-1)*tileSize, z, tileSize, tileSize, level)

		if lovr.mouse.isDown(1) then
			nLevel=level
			--changelevel() 
			--gamestatus="game"
			--timer=0
			--stepdone=0
		end
	end

	drawonlytitles(x,y)
	
------------------------------------------------
-- buttons
------------------------------------------------
--[[

	-- quit

	local hovered = isButtonHovered (exitButton)
	drawButton (exitButton, hovered,exittext)

	if hovered and lovr.mouse.isDown(1) then 
		lovr.timer.sleep( 0.5 )
		lovr.event.quit()
	end


	-- options

	hovered = isButtonHovered (optionsButton)
	drawButton (optionsButton, hovered,optionstext)

	if hovered and lovr.mouse.isDown(1) then 
		if lovr.mouse.isDown(1) then 
			gamestatus="options" 
		end
	end


	--credits

	hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditstext)

	if hovered and lovr.mouse.isDown(1) then 
		if lovr.mouse.isDown(1) then
			lovr.timer.sleep( 0.2 )
			gamestatus="credits" 
		end
	end
	--]]
	
	--[[
	--Music player
if musicplayerunlocked==true then
	hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)

	if hovered and lovr.mouse.isDown(1) then 
		if lovr.mouse.isDown(1) then
			music:stop()
			lovr.timer.sleep( 0.5 )
			gamestatus="music"
		end
	end
end

	--Level editor
if leveleditorunlocked==true then
	hovered = isButtonHovered (leditorButton)
	drawButton (leditorButton, hovered,leditortext)

	if hovered and lovr.mouse.isDown(1) then 
		if lovr.mouse.isDown(1) then
			music:stop()
			gamestatus="leditor"
		end
	end
end	
	-- extras
if extrasunlocked==true then
	hovered = isButtonHovered (extrasButton)
	drawButton (extrasButton, hovered,extrastext)

	if hovered and lovr.mouse.isDown(1) then 
		if lovr.mouse.isDown(1) then
			 music:stop()
			gamestatus="extras" 
		end
	end
end	

if endmenuunlocked==true then	
	-- Ends

	hovered = isButtonHovered (endButton)
	drawButton (endButton, hovered,endtext)

	if hovered and lovr.mouse.isDown(1) then 
		if lovr.mouse.isDown(1) then
			  music:stop()
			  require ('game/ends/endmenu')		-- Load ends menu
			  endmenuload()
				gamestatus="endmenu"
		end
	end
end

--]]
end

function drawonlytitles(x,y)
-- change shadow light from green to yellow when mouse hover spheres
	--[[if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end
	--]]
		
	  --Fish House levels 1-8 
		if x>10 and x<16 and y>0 and y<12 then drawfishhousesphere(x,y) end
		--1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 then drawshipwreckssphere(x,y) end
	    --2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 then drawsilvershipsphere(x,y) end
       --3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 then drawcitydeepsphere(x,y) end
		--4. UFO	--level 52-58
		if y>8 and y<10 and x>4 and x<14 then drawufosphere(x,y) end
	   --5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 then drawcoralreefsphere(x,y) end
	    --6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 then drawtreasurecavesphere(x,y) end
		--7. Dump--level 38-44
		if x>12 and x<14 and y>8 and y<18 then drawdumpsphere(x,y) end
		--8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 then drawsecretcomputersphere(x,y) end
		-- 9. NG
		if y>11 and y<13 and x>3 and x<13 then drawsecretsphere(x,y) end
end
