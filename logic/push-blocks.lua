-- License CC0 (Creative Commons license) (c) darkfrei, 2022
-- push-blocks

 local tileSize = gridSize

function lovrload()

require("levels/changelevel")
changelevel()

lovr.graphics.setBackgroundColor(0.146, 0.73, 0.73)

local pb = pushblocks()			-- game logic
	pb:load (level)
end

function pushblocks()

local path = 'assets/'

pb = {}

local function setBlockOutline (block)
	local m = {} -- map of outlines
	local tiles = block.tiles -- list of tiles as {x1,y1, x2,y2, x3,y3 ...}
	for i = 1, #tiles, 2 do
		local x, y = tiles[i], tiles[i+1]
		if not m[y] then m[y] = {} end
		if not m[y][x] then 
			m[y][x] = {v=true, h=true} 
		else
			m[y][x].v = not m[y][x].v
			m[y][x].h = not m[y][x].h
		end
		
		if not m[y][x+1] then 
			m[y][x+1] = {v=true, h=false} 
		else
			m[y][x+1].v = not m[y][x+1].v
		end
		
		if not m[y+1] then m[y+1] = {} end
		if not m[y+1][x] then 
			m[y+1][x] = {v=false, h=true} 
		else
			m[y+1][x].h = not m[y+1][x].h
		end
	end
	local lines = {}
	for y, xs in pairs (m) do
		for x, tabl in pairs (xs) do
			if m[y][x].v then
				table.insert (lines, {x,y, x,y+1})
			end
			if m[y][x].h then
				table.insert (lines, {x,y, x+1,y})
			end
		end
	end
	block.lines = lines
end

function pb:load (level)
--	print (level.name)
	--local width, height = lovr.graphics.getDimensions()
	local width, height = lovr.graphics.getWidth(), lovr.graphics.getHeight()
	
	--local width, height = 1920, 1080
	self.map = level.map
	self.gridWidth = level.w
	self.gridHeight = level.h
	--self.gridSize = math.min(width/(level.w), height/(level.h))
	self.gridSize = 50
	--[[print ('gridWidth: ' .. self.gridWidth, 
		'gridHeight: ' .. self.gridHeight,
		'gridSize: ' .. self.gridSize)--]]
	
	tileSize = self.gridSize
	gridSize=self.gridSize				-- turns self.gridSize into a global variables needed to reset zoom values on game/mainfunctions/mouseinput.lua
	self.blocks = level.blocks
	self.areas = level.areas
	
	--pb:agentsload(level)
	
	self.blocks = level.blocks
	for i, block in ipairs (self.blocks) do
		setBlockOutline (block)
	end
	self.agents = level.agents
	for i, agent in ipairs (self.agents) do
		setBlockOutline (agent)
		local filename = path..agent.name..'.png'
		--local info = lovr.filesystem.isFile(filename)
		if info then
			local image = lovr.graphics.newImage(filename)
			agent.image = image
			local width, height = image:getDimensions( )
			local scale = self.gridSize/(width/agent.w)
			
			agent.image_scale = scale
			
			agent.image_sx = scale
			agent.image_sy = scale
			agent.image_ox = image:getWidth()/2
			agent.image_oy = image:getHeight()/2
			agent.image_dx = agent.image_ox*agent.image_scale
			agent.image_dy = agent.image_oy*agent.image_scale
		end
	end
	self.activeAgentIndex = 1
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true

		
end

function pb:switchAgent ()
	self.agent.active = false
	local index = self.activeAgentIndex + 1
	if index > #self.agents then
		index = 1
	end
	self.activeAgentIndex = index
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
end


local function isValueInList (value, list)
		for i, element in ipairs (list) do
		if element == value then 
			return true 
		end
	end
	return false
end

--[[The pb:isBlockToMapCollision function is used to determine if there is a collision between a block and the game map when the block is moved by a certain amount in the x and y directions (dx and dy). The function takes a block parameter, which is an object that represents the block, and extracts its x and y coordinates and a list of tiles that make up the block (block.tiles). The function then checks if any of the tiles in the block overlap with a tile in the game map after the block is moved by dx and dy. If there is a collision, the function returns true, otherwise it returns nil.--]]
function pb:isBlockToMapCollision(block, dx, dy)
    local x, y = block.x, block.y
    local map = self.map
    for i = 1, #block.tiles - 1, 2 do
        local mapX = x + block.tiles[i] + dx
        local mapY = y + block.tiles[i + 1] + dy
        if map[mapY] and map[mapY][mapX] and map[mapY][mapX] ~= 6 then	-- do not return true for value 6 which are exit areas
            return true -- Collision with a non-exit area
        end
    end
end

-- This function  is a local function that takes a block block and a list of metablocks metablocks as inputs. It checks if the block is in any of the metablocks and returns true if it is, false otherwise.
local function isBlockInMetablocks (block, metablocks)
    local x, y = block.x, block.y
    local map = map
    --	metablock is list of blocks
	for i, metablock in ipairs (metablocks) do
		if isValueInList (block, metablock) then
			return true
		end
	end
end

-- This function akes a block and its displacement dx and dy and returns a list of blocks that collide with the given block after the displacement. It loops over all blocks in the game and checks if they collide with the given block, and if they do, it adds them to the list of colliding blocks.
function pb:getCollisionBlocksFlat (blockA, dx, dy)
	local blocks = {}
	for i, blockB in ipairs (self.blocks) do 
		if isValueInList (blockB, blocks) then
			-- do nothing
		elseif blockA == blockB then
			-- do nothing
		elseif self:isBlockToBlockCollision (blockA, blockB, dx, dy) then
			table.insert (blocks, blockB)
			
		end
	end
	return blocks
end

-- This function  takes a block and a list of blocks called metablock and returns a list of blocks that form a connected "metablock" with the given block. It recursively checks the block's neighbors and their neighbors to see if they are connected and adds them to the metablock list if they are. If any of the blocks in the metablock list cannot be connected, the function returns false.
function pb:metablockCreation (blockA, metablock)
	-- falling
	local dx, dy = 0, 1
	if self:isBlockToMapCollision (blockA, dx, dy) then 
		-- not falling :)
		--blockA.text = "m1"			-- text disabled
		return false 
	end
	if self:isCollisionBlockToAllAgents (blockA, dx, dy) then 
		-- not falling, on the fish
		--blockA.text = "f1"			-- text disabled
		return false 
	end
	-- neighbour collision blocks
	local blocks = pb:getCollisionBlocksFlat (blockA, dx, dy)
		
	for i, blockB in ipairs (blocks) do
		if isValueInList (blockB, metablock) then
--			-- do nothing
		else
			table.insert (metablock, blockB)
			if not self:metablockCreation (blockB, metablock) then
--				blockA.text = "d1"
				return false
			end
		end
	end
	return metablock
end


-- This function takes a block and returns a new list of blocks that form a falling "metablock" with the given block. It calls pb:metablockCreation to create the metablock list and returns it only if the block is falling, i.e., it is not colliding with the map or any agents.
function pb:newMetablock (block)
	local metablock = {block}
	-- returns falling metablocks only
	if self:metablockCreation (block, metablock) then
		return metablock
	end
end



--This function checks if there is a rough collision between two blocks, where rough collision means that the function is only interested in knowing if the two blocks overlap in any way, but it does not return information about the specific area of overlap. The function receives two blocks as input, blockA and blockB, and also the values dx and dy that indicate a displacement of blockA in the x and y directions, respectively. The function calculates the positions and dimensions of the two blocks based on these values, and then checks if the two blocks overlap in any way. If there is an overlap, the function returns true, otherwise, it returns false.
function pb:isBlockToBlockCollisionRough (blockA, blockB, dx, dy)
	local x1, y1 = blockA.x + dx, blockA.y + dy
	local h1, w1 = blockA.h, blockA.w
	local x2, y2 = blockB.x, blockB.y
	local h2, w2 = blockB.h, blockB.w
	if x1 < x2+w2 and x2 < x1+w1 and
		y1 < y2+h2 and y2 < y1+h1 then
		return true	
	end
	return false
end

--The function checks for collisions between each tile in blockA and each tile in blockB by comparing their x and y positions. If any two tiles have the same x and y position, the function returns true to indicate a collision. If no collisions are found, the function returns false.

function pb:isBlockToBlockCollisionFine (blockA, blockB, dx, dy)
	-- fine tile to tile collision detection
	-- check if blockA moves to dx, dy an collides with blockB
	local xA, yA = blockA.x + dx, blockA.y + dy
	local xB, yB = blockB.x, blockB.y
	local tilesA = blockA.tiles
	local tilesB = blockB.tiles
	for i = 1, #tilesA-1, 2 do
		local dXA, dYA = tilesA[i], tilesA[i+1]
		for j = 1, #tilesB-1, 2 do
			local dXB, dYB = tilesB[j], tilesB[j+1]
			if (xA+dXA == xB+dXB) and (yA+dYA == yB+dYB) then
				-- same x AND same y means collision
				return true
			end
		end
	end
	return false
end


--[[This is a function in a game logic that checks if two blocks collide after one of them moves by a specified amount of displacement (dx, dy). The function first checks for a rough collision using the isBlockToBlockCollisionRough function. If there is no rough collision, it returns false to indicate that there is no collision. If there is a rough collision, the function checks for fine-grained tile-to-tile collision using the isBlockToBlockCollisionFine function. If there is a collision, the function returns true, otherwise it returns false.--]]
function pb:isBlockToBlockCollision (blockA, blockB, dx, dy)
	-- check if blockA moves to dx, dy an collides with blockB
	-- rough
	if not self:isBlockToBlockCollisionRough (blockA, blockB, dx, dy) then
		return false
	elseif self:isBlockToBlockCollisionFine (blockA, blockB, dx, dy) then
	-- fine
		return true
	end
	return false
end


function pb:isBlockToBlockCollision (blockA, blockB, dx, dy)
	-- fine tile to tile collision detection
	-- check if blockA moves to dx, dy an collides with blockB
	local xA, yA = blockA.x + dx, blockA.y + dy
	local xB, yB = blockB.x, blockB.y
	local tilesA = blockA.tiles
	local tilesB = blockB.tiles
	for i = 1, #tilesA-1, 2 do
		local dXA, dYA = tilesA[i], tilesA[i+1]
		for j = 1, #tilesB-1, 2 do
			local dXB, dYB = tilesB[j], tilesB[j+1]
			if (xA+dXA == xB+dXB) and (yA+dYA == yB+dYB) then
				-- same x AND same y means collision
				return true
			end
		end
	end
	return false
end

--testing
function pb:getCollisionExit (Agent)
    for i, agent in ipairs(self.agents) do
        -- Get the coordinates of the cell where the agent is located.
        local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)

        -- Check if the cell has a value of 6 in the grid map.
        if self.map[agentY] and self.map[agentY][agentX] == 6 then
            return true -- Agent collision with exit
        end
    end
    return false -- No collision with exit area
end
	
function pb:getCollisionBlocks (blockA, blocks, dx, dy)
	-- agent to map or block to map collision
	if self:isBlockToMapCollision (blockA, dx, dy) then
		return false
	end
	
	for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return false -- cannot move any agent

		end
	end
	
	for i, block in ipairs (self.blocks) do
		if block == blockA then
			-- self collision: do nothing
		elseif isValueInList (block, blocks) then
			-- block is already in list: do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			-- checks if the agent is strong
			if block.heavy and not self.agent.heavy then
				return false
			end
			table.insert (blocks, block)
			
			-- make it deeper!
			if not self:getCollisionBlocks (block, blocks, dx, dy) then
				return false
			end
		end
	end
	return true
end



function pb:getBlocksToMove (agent, dx, dy)
	local blocks = {}
	local canMove = self:getCollisionBlocks (agent, blocks, dx, dy)
	return blocks, canMove
end


function pb:moveAgent (agent, dx, dy)
	self.agent.x = self.agent.x + dx
	self.agent.y = self.agent.y + dy
end

--This function moves a given block by updating its x and y coordinates by the specified dx and dy values.
function pb:moveBlock (block, dx, dy)
	block.x = block.x + dx
	block.y = block.y + dy
end

--This function moves a list of blocks by calling the moveBlock function for each block in the list. It also triggers an event to indicate that the object has been pushed.
function pb:moveBlocks (blocks, dx, dy)
	for i, block in ipairs (blocks) do
		self:moveBlock (block, dx, dy)
		--eventobbjectpushed(block)			--trigers events when objects are pushed
	end
end

function pb:isCollisionBlockToAllBlocks (blockA, dx, dy)
	for i, block in ipairs (self.blocks) do
		if not (block == blockA) 
		and self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

--This function checks whether a given block collides with any of the other blocks on the map, except for those in the list of exception blocks. It does this by calling the isBlockToBlockCollision function for each block in the list of blocks, and checking if the block is not in the list of exception blocks. If a collision is detected, it returns true. Otherwise, it returns false.
function pb:isCollisionBlockToAllBlocksExcept (blockA, dx, dy, exceptionBlocks)
	for i, block in ipairs (self.blocks) do
		if (block == blockA) then
			-- do nothing
		elseif isValueInList (block, exceptionBlocks) then
			-- do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

function pb:isCollisionAgentToAnyExitArea(agent)
    -- Get the coordinates of the cell where the agent is located.
    --local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)

    -- Debugging: Print agent coordinates and grid value.
    --print("Agent Coordinates (X, Y):", agent.x, agent.y)
    
    if self.map[agent.y] and self.map[agent.y][agent.x] == 6 then
        print("Agent is in an exit area with value 6")
        return true -- Agent is in an exit area with value 6
    else
        print("Agent is NOT in an exit area")
        return false
    end
        
end

-- This function takes a block blockA and two integers dx and dy as inputs. It then checks for a collision between blockA and all agents in the game, assuming blockA moves by dx and dy. If a collision is found, it returns the dead agent. Otherwise, it returns false.
function pb:isCollisionBlockToAllAgents (blockA, dx, dy)
	for i, agent in ipairs (self.agents) do
		if self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return agent -- dead agent :(
		end
	end
	return false
end

-- Function to check if a specific object is in an exit area based on its name.
function pb:isObjectInExitArea(objectName, blocks)
    -- Find the object by its name
    for i, block in ipairs(self.blocks) do
        if block.name == objectName then
            local gridX = block.x
            local gridY = block.y

            -- Debug statements
            --[[print("ObjectName:", objectName)
            print("Block X:", block.x)
            print("Block Y:", block.y)
            print("Grid Size:", self.gridSize)
            print("GridX:", gridX)
            print("GridY:", gridY)
			--]]
            -- Check if the grid cell contains an exit area (value 6)
            if self.map[gridY] and self.map[gridY][gridX] == 6 then
                return true -- Object is in an exit area
            end
        end
    end

    return false -- Object is not in an exit area or not found
end


--check if all agents are in exit areas from the grid.
function pb:areAllAgentsInExitAreas()
    for i, agent in ipairs(self.agents) do
        -- Get the coordinates of the cell where the agent is located.
        --local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)

        -- Check if the cell has a value of 6 in the grid map.
        if not (self.map[agent.y] and self.map[agent.y][agent.x] == 6) then
            return false -- At least one agent is not in an exit area with value 6
        end
    end
    return true
end

--check if all agents are in exit areas from the grid.
function pb:areAllAgentsInExitAreas()
    for i, agent in ipairs(self.agents) do
        -- Get the coordinates of the cell where the agent is located.
        --local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)

        -- Check if the cell has a value of 6 in the grid map.
        if not (self.map[agent.y] and self.map[agent.y][agent.x] == 6) then
            return false -- At least one agent is not in an exit area with value 6
        end
    end
    return true
end

-- This function  takes an agent agent and an area area as inputs, and checks if the agent is currently in the area. It returns true if the agent is in the area, and false otherwise.
function pb:isAgentInArea (agent, area)
	return self:isBlockToBlockCollisionRough (agent, area, 0, 0) -- returns true if AABB 
	-- see https://love2d.org/wiki/BoundingBox.lua
end


-- This function  is a local function that takes a block block and a list of metablocks metablocks as inputs. It checks if the block is in any of the metablocks and returns true if it is, false otherwise.
local function isBlockInMetablocks (block, metablocks)
--	metablock is list of blocks
	for i, metablock in ipairs (metablocks) do
		if isValueInList (block, metablock) then
			return true
		end
	end
end

-- This function akes a block and its displacement dx and dy and returns a list of blocks that collide with the given block after the displacement. It loops over all blocks in the game and checks if they collide with the given block, and if they do, it adds them to the list of colliding blocks.
function pb:getCollisionBlocksFlat (blockA, dx, dy)
	local blocks = {}
	for i, blockB in ipairs (self.blocks) do 
		if isValueInList (blockB, blocks) then
			-- do nothing
		elseif blockA == blockB then
			-- do nothing
		elseif self:isBlockToBlockCollision (blockA, blockB, dx, dy) then
			table.insert (blocks, blockB)
			
		end
	end
	return blocks
end

-- This function  takes a block and a list of blocks called metablock and returns a list of blocks that form a connected "metablock" with the given block. It recursively checks the block's neighbors and their neighbors to see if they are connected and adds them to the metablock list if they are. If any of the blocks in the metablock list cannot be connected, the function returns false.
function pb:metablockCreation (blockA, metablock)
	-- falling
	local dx, dy = 0, 1
	if self:isBlockToMapCollision (blockA, dx, dy) then 
		-- not falling :)
		--blockA.text = "m1"			-- text disabled
		return false 
	end
	if self:isCollisionBlockToAllAgents (blockA, dx, dy) then 
		-- not falling, on the fish
		--blockA.text = "f1"			-- text disabled
		return false 
	end
	-- neighbour collision blocks
	local blocks = pb:getCollisionBlocksFlat (blockA, dx, dy)
		
	for i, blockB in ipairs (blocks) do
		if isValueInList (blockB, metablock) then
--			-- do nothing
		else
			table.insert (metablock, blockB)
			if not self:metablockCreation (blockB, metablock) then
--				blockA.text = "d1"
				return false
			end
		end
	end
	return metablock
end

-- This function takes a block and returns a new list of blocks that form a falling "metablock" with the given block. It calls pb:metablockCreation to create the metablock list and returns it only if the block is falling, i.e., it is not colliding with the map or any agents.
function pb:newMetablock (block)
	local metablock = {block}
	-- returns falling metablocks only
	if self:metablockCreation (block, metablock) then
		return metablock
	end
end


function pb:fallBlocks ()
	-- no horizontal speed, but positive (down) vertical speed
	local dx, dy = 0, 1
	local metablocks = {}
	for i, block in ipairs (self.blocks) do
		block.text = nil
	end
	for i, block in ipairs (self.blocks) do
		if not isBlockInMetablocks (block, metablocks) then
			-- new metablock
			local metablock = self:newMetablock (block)
			if metablock then
				table.insert(metablocks, metablock)
			end
		end
	end
--	print ('#metablocks', #metablocks)
	
	for i = 1, self.gridHeight do
		local somethingMoved = false
		for iMetablock = #metablocks, 1, -1 do -- backwards
			local metablock = metablocks[iMetablock]
--			print ('metablock', iMetablock, 'blocks:', #metablock)
			local metablockFalling = true
			
			for j, block in ipairs (metablock) do
				if self:isBlockToMapCollision (block, dx, dy) then
					-- not falling
					--block.text = "map"				-- text disabled
					metablockFalling = false
				elseif self:isCollisionBlockToAllBlocksExcept (block, dx, dy, metablock) then
					-- collision to block: not falling
					--block.text = "fest"				-- text disabled
					metablockFalling = false
				elseif self:isCollisionBlockToAllBlocks (block, dx, dy) then
					-- collision to metablock
					--block.text = "fallM"			-- text disabled
				elseif self:isCollisionBlockToAllAgents (block, dx, dy) then
					local deadAgent = self:isCollisionBlockToAllAgents (block, dx, dy)
					--block.text = 'on dead'			-- text disabled
					deadAgent.dead = true
					metablockFalling = false
				else
					--block.text = "fallS"			-- text disabled
--					block moved only if metablock moved
					
					--eventsblockfalling(block)			--trigger events when an object falls
				end
			end
			if metablockFalling then
				self:moveBlocks (metablock, dx, dy)
				somethingMoved = true
			else
				-- the metablock is not falling anymore
				table.remove(metablocks, iMetablock)
			end
		end
		if not somethingMoved then 
			-- nothing to move
			return 
		end
	end
end

function pb:mainMoving (dx, dy)
	local agent = self.agent -- active agent
	if dx > 0 then 
		agent.direction = "right"
		if agent.name=="fish-4x2" then
			agentDirection="right"
		elseif agent.name=="fish-3x1" then
			agent2Direction="right"
		end
	elseif dx < 0 then
	
		agent.direction = "left"
		if agent.name=="fish-4x2" then
			agentDirection="left"
		elseif agent.name=="fish-3x1" then
			agent2Direction="left"
		end
	end
	local blocks, canMove = self:getBlocksToMove (agent, dx, dy)
	if canMove then
		self:moveAgent (agent, dx, dy)
		self:moveBlocks (blocks, dx, dy)
		self:fallBlocks ()
		
		local fallingBlocks = self:fallBlocks ( ) 
		
		local allFishOnExit=self:areAllAgentsInExitAreas () -- true if both are in area
		if allFishOnExit then
			-- on some levels some task need to be fulfilled in order to complete the level,
			-- we make sure we are not on such levels before checking if agents are in exit areas
			if not (nLevel==19) and not (nLevel==29) and not (nLevel==37) and not (nLevel==44) and not (nLevel==51) and not (nLevel==58) and not (nLevel==70) and not (nLevel==77) then
				nLevel=nLevel+1
				changelevel()
				lovr.graphics.clear()
				pb:load (level)
				loadlevelassets()
				levelComplete () -- your logic for level completing here
			elseif nLevel==19 or nLevel==29 or nLevel==37 or nLevel==44 or nLevel==51 or nLevel==58 or nLevel==70 or nLevel==77 then	-- do nothing, the level is completed if a god is in the exit area (see condition below)
				eventAgentInExitButObjectNot()	-- this function plays a random border sentence when the fish tries to exit the level without completing a task
			end
		end
		
		if nLevel==19 then	-- on level 19 Poseidon or neptun needs to be in the exit area in order to complete the level
			local isNeptunInExitArea = pb:isObjectInExitArea(neptun)
			local isPoseidonInExitArea = pb:isObjectInExitArea(poseidon)

					if isNeptunInExitArea or isPoseidonInExitArea then
						print(objectName .. " A god is in the exit area, level completed")
						levelComplete ()
					elseif pb:isCollisionAgentToAnyExitArea(agent) then
						eventAgentInExitButObjectNot()
					end
		elseif nLevel==29 then
			local isThePlugInExitArea = pb:isObjectInExitArea(plug)
			if isThePlugInExitArea then
				print(objectName .. " The plug is in the hole, well done!")
				levelComplete ()
			elseif pb:isCollisionAgentToAnyExitArea(agent) then
						eventAgentInExitButObjectNot()
			end
		elseif nLevel==37 then
			local isTelepaticTurtleInExitArea = pb:isObjectInExitArea(turtle)
				if isTelepaticTurtleInExitArea then
					print(objectName .. " The telepatic turtle is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==44 then
			local isRadioactiveWasteInExitArea = pb:isObjectInExitArea(barel)
				if isRadioactiveWaste then
					print(objectName .. " The radioactive waste is in exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==51 then
			local isTheMapInExitArea = pb:isObjectInExitArea(mapab)
				if isTheMapInExitArea then
					print(objectName .. " The map is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==58 then
			local isTheSquirrelExitArea = pb:isObjectInExitArea(pohon)
				if isTheSquirrelExitArea then
					print(objectName .. " The squirrel is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel == 64 then
			totalHolyGrails = 25  -- Total number of Holy Grails
			holyGrailsInExitArea = 0  -- Initialize count to zero

			-- Check each Holy Grail
			for i = 1, totalHolyGrails do
				local grailName = "gral" .. i
				if pb:isObjectInExitArea(grailName, level.blocks) then
					holyGrailsInExitArea = holyGrailsInExitArea + 1  -- Increment count if in exit area
				end
			end

			--print("Number of Holy Grails in the exit area:", holyGrailsInExitArea)

			if holyGrailsInExitArea == totalHolyGrails then
				print("All Holy Grails are in the exit area, level completed!")
				levelComplete()
			elseif pb:isCollisionAgentToAnyExitArea(agent) then
				eventAgentInExitButObjectNot()
			end
		

		elseif nLevel==70 then
			local isTheFloppyDiskInExitArea = pb:isObjectInExitArea(floppy)
				if isTheFloppyDiskInExitArea then
					print(objectName .. " The floppy disk is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==77 then
			local isTheLinuxUserInExitArea = pb:isObjectInExitArea(linuxak1)
			local isTheLinuxUser2InExitArea = pb:isObjectInExitArea(linuxak2)
				if isTheLinuxUserInExitArea or isTheLinuxUser2InExitArea then
					print(objectName .. " The linux user is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		end
		
	end
end

function lovr.gamepadpressed(joystick, button)
	local dx, dy = 0, 0 -- Initialize dx and dy variables

	-- Check the button pressed and set the corresponding dx and dy values
	if button == "dpleft" then
		dx = -1
	elseif button == "dpright" then
		dx = 1
	elseif button == "dpup" then
		dy = -1
	elseif button == "dpdown" then
		dy = 1
	elseif button == "a" then
		pb:switchAgent()
	elseif button == "b" then
		Talkies.clearMessages()
		talkies = false
	elseif button == "y" then
		if helpison == "yes" then
			helpison = "no"
		elseif helpison == "no" then
			helpison = "yes"
		end
	elseif button == "start" then
		gamestatus = "levelselection"
	elseif button == "back" then
		shader2 = false
		gamestatus = "options"
	end

	pb:mainMoving(dx, dy) -- Move the agent or blocks based on dx and dy
end


---------------------------------------------------------------------------------------------------
-- draw
---------------------------------------------------------------------------------------------------

function pb:drawBackgroundGrid ()
	local gridSize = self.gridSize
	local gridWidth = self.gridWidth
	local gridHeight = self.gridHeight
	lovr.graphics.setLineWidth(1)
	lovr.graphics.setColor(0.3,0.4,0.4)
	for i = 0, gridWidth do
		--lovr.graphics.line (i*gridSize, 0,0, i*gridSize, gridHeight*gridSize,0)
					--line (i*gridSize, 0, i*gridSize, gridHeight*gridSize)
	end
	for i = 0, gridHeight do
		--lovr.graphics.line (0, i*gridSize,0, gridWidth*gridSize, i*gridSize,0)
					--line (0, i*gridSize, gridWidth*gridSize, i*gridSize)
	end

end

function pb:drawMap ()
lovr.graphics.transform(-32,20,-32, 0.05, 0.05, 0.05,9.43,60,0,1)
 local map = self.map
 local tileSize = self.gridSize
 lovr.graphics.setLineWidth(2)

 for y, xs in ipairs (map) do
  for x, value in ipairs (xs) do
   -- value is boolean: true or false
   if value==true and not (nLevel==1) then -- map tile
    
   
    -- beware of -1
      lovr.graphics.setColor(0,1,1)
  
   elseif value==6 then -- map tile
   lovr.graphics.setColor(1,1,0,1)
   lovr.graphics.cube ('line', (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
							--line ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
   --borders
   elseif value=="border" then -- map tile
			
				lovr.graphics.setColor(1,1,1)
					if nLevel==1 then lovr.graphics.cube (level1bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==2 then lovr.graphics.cube (level2block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==3 then
					if level3bordermat==nil then level3bordermat = lovr.graphics.newMaterial(level3bordertex,1,1,1,1) end -- prevent game from crashing
						lovr.graphics.cube (level3bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==4 then lovr.graphics.cube (level4block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==5 then lovr.graphics.cube (level5block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==6 then lovr.graphics.cube (level6block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==7 then lovr.graphics.cube (level7block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==8 then lovr.graphics.cube (level8block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==9 then lovr.graphics.cube (level9block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==10 then lovr.graphics.cube (level10block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==11 then lovr.graphics.cube (level11bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==12 then lovr.graphics.cube (level12block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==13 then lovr.graphics.cube (level13block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==14 then lovr.graphics.cube (level14block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==15 then lovr.graphics.cube (level15block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==16 then lovr.graphics.cube (level16block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==17 then lovr.graphics.cube (level17block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==18 then lovr.graphics.cube (level18block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==19 then lovr.graphics.cube (level19block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
								
				elseif nLevel==20 then lovr.graphics.cube (level20block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==21 then lovr.graphics.cube (level21block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==22 then lovr.graphics.cube (level22block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==23 then lovr.graphics.cube (level23block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==24 then lovr.graphics.cube (level24block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==25 then lovr.graphics.cube (level25block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==26 then lovr.graphics.cube (level26block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==27 then lovr.graphics.cube (level27block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==28 then lovr.graphics.cube (level28block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==29 then lovr.graphics.cube (level29block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
								
				elseif nLevel==30 then lovr.graphics.cube (level30block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==31 then lovr.graphics.cube (level31block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==32 then lovr.graphics.cube (level32block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==33 then lovr.graphics.cube (level33block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==34 then lovr.graphics.setColor(0.8,0.5,1,1)	lovr.graphics.cube (level34block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==35 then lovr.graphics.cube (level35block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==36 then lovr.graphics.cube (level36block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==37 then lovr.graphics.cube (level37block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==38 then lovr.graphics.cube (level38block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==39 then lovr.graphics.cube (level39block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
								
				elseif nLevel==40 then lovr.graphics.cube (level40block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==41 then lovr.graphics.cube (level41block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==42 then lovr.graphics.cube (level42block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==43 then lovr.graphics.cube (level43block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==44 then lovr.graphics.cube (level44block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==45 then lovr.graphics.cube (level45block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==46 then lovr.graphics.cube (level46block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==47 then lovr.graphics.cube (level47block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==48 then lovr.graphics.cube (level48block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==49 then lovr.graphics.cube (level49block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
								
				elseif nLevel==50 then lovr.graphics.cube (level50block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==51 then lovr.graphics.cube (level51block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==52 then lovr.graphics.cube (level52block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==53 then lovr.graphics.cube (level53block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==54 then lovr.graphics.setColor(0,1,0,1)	lovr.graphics.cube (level54block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==55 then lovr.graphics.setColor(1,0,0,1)	lovr.graphics.cube (level55block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==56 then lovr.graphics.setColor(1,0,1,1)	lovr.graphics.cube (level56block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==57 then lovr.graphics.setColor(1,1,0,1)	lovr.graphics.cube (level57block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==58 then lovr.graphics.setColor(0,1,1,1)	lovr.graphics.cube (level58block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==59 then lovr.graphics.cube (level59block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				
				
				elseif nLevel==60 then lovr.graphics.cube (level60block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==61 then lovr.graphics.cube (level61block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==62 then lovr.graphics.cube (level62block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==63 then lovr.graphics.cube (level63block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==64 then lovr.graphics.setColor(1,0,1,1)	lovr.graphics.cube (level64block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==65 then lovr.graphics.cube (level65block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==66 then lovr.graphics.cube (level66block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==67 then lovr.graphics.cube (level67block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==68 then lovr.graphics.cube (level68block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==69 then lovr.graphics.cube (level69block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				
				elseif nLevel==70 then lovr.graphics.cube (level70block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==71 then lovr.graphics.cube (level71block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==72 then lovr.graphics.cube (level72block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==73 then lovr.graphics.cube (level73block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==74 then lovr.graphics.cube (level74block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==75 then lovr.graphics.cube (level75block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==76 then lovr.graphics.cube (level76block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==77 then lovr.graphics.cube (level77block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==78 then lovr.graphics.cube (level78block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==79 then lovr.graphics.cube (level79block, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				
				
				elseif nLevel==30 then
						lovr.graphics.setColor(0.5,1,0.5,1)
						level31bordermat = lovr.graphics.newMaterial(level31bordertex,1,1,1,1)
						lovr.graphics.cube (level31bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==31 then
						lovr.graphics.setColor(0.5,1,0.5,1)
						level31bordermat = lovr.graphics.newMaterial(level31bordertex,1,1,1,1)
						lovr.graphics.cube (level31bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==34 then
						lovr.graphics.setColor(0.9,0.3,0.2,1)
						if level34bordermat==nil then level34bordermat = lovr.graphics.newMaterial(level34bordertex,1,1,1,1) end -- prevent game from crashing
						lovr.graphics.cube (level34bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==52 then
						if level52bordermat==nil then level52bordermat = lovr.graphics.newMaterial(level52bordertex,1,1,1,1) end -- prevent game from crashing
						lovr.graphics.cube (level52bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				elseif nLevel==61 then
						lovr.graphics.setColor(0.3,0.6,1,1)
						if level61bordermat==nil then level61bordermat = lovr.graphics.newMaterial(level61bordertex,1,1,1,1) end -- prevent game from crashing
						lovr.graphics.cube (level61bordermat, (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				else lovr.graphics.setColor(1,0,0) lovr.graphics.cube ('fill', (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
				end
			
	elseif value==false and not (nLevel==1) then -- free tile
			--lovr.graphics.setColor(0,0,1,0.3) lovr.graphics.cube ('line', (x-1)*tileSize, (y-1)*tileSize,0, tileSize)
	
					     
   end
  end
 end
 end
 
 
function pb:drawOutline  (block)

local lines = block.lines
 local tileSize = self.gridSize
  local x, y = block.x-1, block.y-1
   for i, line in ipairs (lines) do
   --lovr.graphics.line ((x+line[1])*tileSize, (y+line[2])*tileSize,0, (x+line[3])*tileSize, (y+line[4])*tileSize,0)
			   --line ((x+line[1])*tileSize, (y+line[2])*tileSize, (x+line[3])*tileSize, (y+line[4])*tileSize)
   end
end    


function pb:drawBlock (block)

 local x, y = block.x, block.y
 local tileSize = self.gridSize
 for i = 1, #block.tiles-1, 2 do
  local dx, dy = block.tiles[i], block.tiles[i+1]
  -- beware of -1
		drawobjectsassets(block,x,y,dx,dy,tileSize,i,tilesize,dividx,dividy,pb)
	end
end


function pb:drawBlocks ()
		
 for i, block in ipairs (self.blocks) do
  -- draw filled block

  self:drawBlock (block)
  
  -- outline
  if block.heavy then
   lovr.graphics.setColor(0,1,1)
  else
   lovr.graphics.setColor(0,1,0)
  end
  self:drawOutline  (block)
 end
end

function pb:drawDeadAgent (agent)
	local tileSize = self.gridSize 
	local x = (agent.x-1)*tileSize
	local y = (agent.y-1)*tileSize
	local w = agent.w*tileSize
	local h = agent.h*tileSize
	
	lovr.graphics.line (x, y,0, x+w, y+h,0)
				--line (x, y, x+w, y+h)
	lovr.graphics.line (x, y+h,0, x+w, y,0)
				--line (x, y+h, x+w, y)
end


local function drawTexture (agent, tileSize)
 
 --local sx = agent.image_sx
 --if agent.direction and agent.direction == "left" then
  --sx = -agent.image_sx
 --end

drawplayerassets(agent, tileSize,sx)
  
  
end
local function drawagentrectangle(agent, tileSize)
 local sx = agent.image_sx
 --lovr.graphics.cube ('fill',(agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy,0, agent.image_dx*2)
						  --rect ((agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy, agent.image_dx*2, agent.image_dy*2)
end                         



-- this function is in exclusively in charge of loading the position of agents and blocks when load game function is triggered
function pb:loadlevel ()
 -- Load level logic here
    
	-- this section loads the position of blocks
	local tempblockname
	
		for i, block in ipairs (self.blocks) do
			--check if blocks are saved and assign the values from the files to the local variable "tempblockname / tempblocknamex tempblocknamey)
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if lovr.filesystem.isFile(nLevel .. block.name .. '.txt') then
				tempblockname =  nLevel .. block.name .. '.txt'
				tempblockname = lovr.filesystem.read( tempblockname ) 
				block.name = tempblockname
			end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if lovr.filesystem.isFile(nLevel .. block.name .. 'x' .. '.txt') then
				tempblocknamex = nLevel .. block.name .. 'x' .. '.txt'
				tempblocknamex = lovr.filesystem.read( tempblocknamex )
				block.x = tonumber(tempblocknamex)
			end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if lovr.filesystem.isFile(nLevel .. block.name .. 'y' .. '.txt') then
				tempblocknamey = nLevel .. block.name .. 'y' .. '.txt'
				tempblocknamey = lovr.filesystem.read( tempblocknamey )
				block.y = tonumber(tempblocknamey)
			end
			
		end
	
	
	-- this section loads the position of agents (fish)
	local tempagentname
		for i, agent in ipairs (self.agents) do
			--check if the file exist to prevent the game from crashing due to missing saved data
			--if lovr.filesystem.isFile(nLevel .. agent.name .. '.txt') then
				tempagentname =  nLevel .. agent.name .. '.txt'
				tempagentname = lovr.filesystem.read( tempagentname )
				agent.name = tempagentname
			--end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			--if lovr.filesystem.isFile(nLevel .. agent.name .. 'x' .. '.txt') then
				tempagentnamex = nLevel ..agent.name .. 'x' .. '.txt'
				tempagentnamex = lovr.filesystem.read( tempagentnamex )
				agent.x = tonumber(tempagentnamex)	
			--end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			--if lovr.filesystem.isFile(nLevel .. agent.name .. 'y' .. '.txt') then
				tempagentnamey = nLevel ..agent.name .. 'y' .. '.txt'
				tempagentnamey = lovr.filesystem.read( tempagentnamey )
				agent.y = tonumber(tempagentnamey)
			--end

		end
		
		--if lovr.filesystem.isFile('1chair1.txt') then	-- this checks that game has been saved and then plays the dialogs
				if nLevel==1 then
					pipefalled=true	-- this prevents the dialogs from not playing on level 1 when a game is saved
					--Obey.lev1()
				end
		--end
end

-- this function is in exclusively in charge of saving the position of agents and blocks when load game function is triggered
function pb:savelevel ()
	-- this section saves the position of blocks
	for i, block in ipairs (self.blocks) do
		block.x= tostring(block.x)
		block.y= tostring(block.y)
		 success, message = lovr.filesystem.write( nLevel .. block.name .. ".txt", block.name)
		 success, message = lovr.filesystem.write( nLevel .. block.name .. "x" .. ".txt", block.x)
		 success, message = lovr.filesystem.write( nLevel .. block.name .. "y" .. ".txt", block.y)
	end
	
	-- this section saves the position of agents
	for i, agent in ipairs (self.agents) do
		agent.x= tostring(agent.x)
		agent.y= tostring(agent.y)
		 success, message = lovr.filesystem.write( nLevel .. agent.name .. ".txt", agent.name)
		 success, message = lovr.filesystem.write( nLevel .. agent.name .. "x" .. ".txt", agent.x)
		 success, message = lovr.filesystem.write( nLevel .. agent.name .. "y" .. ".txt", agent.y)
	end
	
	    -- Save level logic here
    --saveGameState(self) -- Save the current state to the game history
end

function pb:drawAgents (agent)

 local activeAgent = self.agent
 local tileSize = self.gridSize
 gridSize=self.gridSize
 for i, agent in ipairs (self.agents) do

  if agent.image then
   if agent == activeAgent then
    lovr.graphics.setColor(1,1,1)
  
    drawTexture (agent, tileSize)
    
   else
    lovr.graphics.setColor(0.7,0.7,0.7)
    
    --drawTexture (agent, tileSize)
   end
  else
   if agent == activeAgent then
    lovr.graphics.setColor(1,1,1)
    self:drawBlock (agent)
    
    local x, y = agent.x, agent.y
    lovr.graphics.setColor (0, 0, 0)
    --print (agent.x..' '..agent.y, (agent.x-1)*tileSize, (agent.y-1)*tileSize)
   else
    lovr.graphics.setColor(0.75,0.75,0.5)
    self:drawBlock (agent)
   end
   
   -- outline
   
   if agent.heavy then
    lovr.graphics.setColor(0,1,1)
   else
	lovr.graphics.setColor(0,1,0)
   end
   self:drawOutline  (agent)
  end

  if agent.dead then
   lovr.graphics.setColor(0,0,0)
   self:drawDeadAgent (agent)
  end
end
end
--drawTexture (agent, tileSize)
return pb

end


--The drawArea(area) function takes an area object as an input parameter and checks whether all the fish are on the exit. If they are, it calls the levelComplete() function to trigger the level completion logic.
function drawArea (area)
 -- no fishes inside
 -- one fish inside, 
 -- all fishes inside
	if allFishOnExit then
		self:levelComplete () -- your logic for level completing here
	end
end

function lovr.gamepadpressed(joystick,button)

  if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
  
   if joystick:isGamepadDown("dpleft") then
    dx = -1
    dy = 0
   elseif joystick:isGamepadDown("dpright") then
    dx = 1
    dy = 0
   elseif  joystick:isGamepadDown("dpup") then
    dx = 0
    dy = -1
   elseif joystick:isGamepadDown("dpdown") then
    dx = 0
    dy = 1
   elseif joystick:isGamepadDown("a")  then
   pb:switchAgent ()
   dx = 0
   dy = 0
   elseif joystick:isGamepadDown("b")  then
   Talkies.clearMessages() talkies=false
   dx = 0
   dy = 0
   elseif joystick:isGamepadDown("y")  then
     if helpison=="yes" then helpison="no"
    elseif helpison=="no" then helpison="yes"
    end
   dx = 0
   dy = 0
   elseif joystick:isGamepadDown("start")  then
   dx = 0
   dy = 0
   gamestatus="levelselection"
   elseif joystick:isGamepadDown("back")  then
   dx = 0
   dy = 0
   shader2=false
   gamestatus="options"
   end
      pb:mainMoving (dx, dy)
  end
  
   if joystick:isGamepad() then
   isgamepad=true
  end
end

function lovr.update(dt)
--[[local JOY_UP = lovr.input.joypad("up")
	local JOY_DOWN = lovr.input.joypad("down")
	local JOY_LEFT = lovr.input.joypad("left")
	local JOY_RIGHT = lovr.input.joypad("right")

	if JOY_UP    == 1 then local dy=dy-1 end
	if JOY_DOWN  == 1 then local dy=dy+1 end
	if JOY_LEFT  == 1 then local dx=dx-1 end
	if JOY_RIGHT == 1 then local dx=dx+1 end
	--]]
	pb:mainMoving (dx, dy)
end

function mydraw()
pb:drawMap ()
pb:drawBlocks ()
pb:drawAgents ()
pb:drawBackgroundGrid()

end
