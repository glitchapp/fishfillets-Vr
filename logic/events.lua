--Events by glitchapp

-- License CC0 (Creative Commons license) (c) glitchapp, 2022

--available functions: eventcollisionblocks, eventagentposition, eventobbjectpushed, eventsblockfalling

--[[
The levelComplete() function is called when a level is completed. It checks if the next level should be unlocked and updates the game status accordingly.
--]]
function onlyonefishisinexit()
	if nLevel==29 then
		if plugisinplace==false then
			--effects
			ithinkwehave:setEffect('myEffect')
			ithinkwehave:play()
		end
	end
end

function levelComplete ()
				if not (nLevel==19)
			and not (nLevel==29)
			and not (nLevel==37)
			and not (nLevel==44)
			and not (nLevel==51)
			and not (nLevel==58)
			and not (nLevel==64)
			and not (nLevel==70)
			and not (nLevel==79)
			
			then
			timer=0
			gamestatus="levelcompleted"
		
		if nLevel==19 then	-- on level 19 Poseidon or neptun needs to be in the exit area in order to complete the level
				local isNeptunInExitArea = pb:isObjectInExitArea(neptun)
				local isPoseidonInExitArea = pb:isObjectInExitArea(poseidong)
				if isNeptunInExitArea or isPoseidonInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==29 then
				local isThePlugInExitArea = pb:isObjectInExitArea(plug)
				if isThePlugInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==37 then
				local isTelepaticTurtleInExitArea = pb:isObjectInExitArea(turtle)
				if sTelepaticTurtleInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==44 then
				local isRadioactiveWasteInExitArea = pb:isObjectInExitArea(barel)
				if isRadioactiveWasteInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==51 then
				local isTheMapInExitArea = pb:isObjectInExitArea(mapab)
				if isTheMapInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==58 then
				local isTheSquirrelExitArea = pb:isObjectInExitArea(pohon)
				if isTheSquirrelExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==64 then
				local isTheHolyGrailInExitArea = pb:isObjectInExitArea("gral18", level.blocks)
				if isTheHolyGrailInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==70 then
				local isTheFloppyDiskInExitArea = pb:isObjectInExitArea(floppy)
				if isTheFloppyDiskInExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		elseif nLevel==77 then
				local isTheLinuxUserInExitArea = pb:isObjectInExitArea(linuxak1)
				local isTheLinuxUser2InExitArea = pb:isObjectInExitArea(linuxak2)
				if isTheLinuxUserInExitArea and isTheLinuxUser2InExitArea then
					print("Level complete End Area")
					levelCompleteEndAreas()
				end
		end
		
end


function levelCompleteEndAreas()
			timer=0
			gamestatus="levelcompleted"

-- Define a table to store level-specific data
local levels = {
    [19] = {
        dialogs = {
            "game/dialogs/talkies19",
            "game/dialogs/en/l19en",
            "game/dialogs/es/l19es",
            "game/dialogs/de/l19de",
            "game/dialogs/fr/l19fr",
            "game/dialogs/pl/l19pl",
            "game/dialogs/sv/l19sv",
            "game/dialogs/sl/l19sl",
            "game/dialogs/ru/l19ru",
        },
        endFunction = Obey.lev19end,
        endScene = "shipwrecksend",
    },
    [29] = {
        dialogs = {
            "game/dialogs/talkies29",
            "game/dialogs/en/l29en",
        },
        endFunction = Obey.lev29end,
        endScene = "cityinthedeepend",
    },
    [37] = {
        dialogs = {
            "game/dialogs/talkies37",
            "game/dialogs/en/l37en",
        },
        endFunction = Obey.lev37end,
        endScene = "coralreefend",
    },
    [44] = {
        dialogs = {
            "game/dialogs/talkies44",
            "game/dialogs/en/l44en",
        },
        endFunction = Obey.lev44end,
        endScene = "barrelend",
    },
    [51] = {
        dialogs = {
            "game/dialogs/talkies51",
            "game/dialogs/en/l51en",
        },
        endFunction = Obey.lev51end,
        endScene = "silversshipend",
    },
    [58] = {
        dialogs = {
            "game/dialogs/talkies58",
            "game/dialogs/en/l58en",
        },
        endFunction = Obey.lev58end,
        endScene = "ufoend",
    },
    [64] = {
        dialogs = {
            "game/dialogs/talkies64",
            "game/dialogs/en/l64en",
        },
        endFunction = Obey.lev64end,
        endScene = "treasurecaveend",
    },
    [70] = {
        dialogs = {
            "game/dialogs/talkies70",
            "game/dialogs/en/l70en",
        },
        endFunction = Obey.lev70end,
        endScene = "secretcomputerend",
    },
    [77] = {
        dialogs = {
            "game/dialogs/talkies77",
            "game/dialogs/en/l77en",
        },
        endFunction = Obey.lev77end,
        endScene = "linuxend",
    },
}



end
end

function saveprogress()

end

function onefishonexit ()
		print("onefish")
end


function eventcollisionblocks(block)
--script that trigger dialogs when small fish try to push the pipe in the first level
				if nLevel==1 and block.name=="steel-pipe-1x81" and pipetouched==false then
							
							
						whatwasthat:stop() 
						ihavenoidea:stop()
						weshouldgoandhave:stop()
						waitimgoingwithyou:stop()
						
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.Icantgetthrough() end
				
					if pipepushed==true then wowyoumovedit:stop() end
				
									pipetouched=true
							
								
				end
				
				--script that trigger dialogs when small fish touch short steal pipe on level 3
				if nLevel==3 and block.name=="shortpipe2" and shortpipetouched==false then
					if language=="en" then
						if accent2=="br" then
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/en/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						elseif accent2=="us" then
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/en-us/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						end
						elseif language=="es" then
						if accent2=="es" then
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/es/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						elseif accent2=="la" then
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/es-la/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						end
						
					end
					if language2=="pl" then
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/pl/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
					elseif language2=="nl" then
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/nl/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
					end
				end
end

function eventagentposition(block,agent)
	-- triggers audio by agent position
				--level 4
	if nLevel==4 and agent.y<10 and aproachcreature==false then
		--if shipwrecksarrogantm:isPlaying() then shipwrecksarrogantm:stop() end
			if language=="en" then
			end
		--print (agent.y)
				if language=="en" then
					if accent=="br" then
								youareanobstacle = love.audio.newSource( "/externalassets/dialogs/level4/youareanobstacle.ogg","stream" )
								youareanobstacle:setEffect('myEffect')
								youareanobstacle:play()
					elseif accent=="us" then
								youareanobstacle = love.audio.newSource( "/externalassets/dialogs/level4/en-us/youareanobstacle.ogg","stream" )
								youareanobstacle:setEffect('myEffect')
								youareanobstacle:play()
					end
				elseif language=="fr" then
								youareanobstacle = love.audio.newSource( "/externalassets/dialogs/level4/fr/youareanobstacle.ogg","stream" )
								youareanobstacle:setEffect('myEffect')
								youareanobstacle:play()
			aproachcreature=true
			end
			
		elseif nLevel==54 and enginepushed==false and aproachengineonce==false and agent.x>25 and agent.x<28 and agent.y<14 and agent.y>10 then
		
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev54aproach() end
							
							aproachengineonce=true
		--elseif nLevel==54 and aproachengineonce==true and agent.x<25 and agent.x>28 and agent.y>14 and agent.y<10 then
							
			
		elseif  nLevel==56 and agent.y>20 and aproachrobodog==false then --level 56
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev56robodog() end
			
			aproachrobodog=true
	end
end


-- this function plays a random border sentence when the fish tries to exit the level without completing a task
function eventAgentInExitButObjectNot()
if timer3==nil then timer3=0 end

if timer3>4 then
	math.randomseed(os.time())
	aleatoryBordersentence = math.random(0,7)
		
				--fish 1
				if aleatoryBordersentence==0 then	--IThink:play()	-- I think we have a job to do here
			elseif aleatoryBordersentence==1 then	--Wedidnt:play()		-- We didn't fulfill our mission yet
			elseif aleatoryBordersentence==2 then	--Theagencyrely:play()	-- The agency relies upon us, we must not fail
			elseif aleatoryBordersentence==3 then	--Certainlyweare:play()	-- Certainly we are not going to run away
				--fish 2
			elseif aleatoryBordersentence==4 then	--IThink2:play()	-- I think we have a job to do here
			elseif aleatoryBordersentence==5 then	--Wedidnt2:play()		-- We didn't fulfill our mission yet
			elseif aleatoryBordersentence==6 then	--Theagencyrely2:play()	-- The agency relies upon us, we must not fail
			elseif aleatoryBordersentence==7 then	--Certainlyweare2:play()	-- Certainly we are not going to run away
			end
	timer3=0
end
end

function eventobbjectpushed(block)
	if soundon==true and block.heavy==true then
	    scrape6:play()		--friction sound
	elseif soundon==true and block.heavy==false then
	    underwatermovement2:play()		--friction sound low
	end
	if soundon==true and nLevel==60 then
		gemsound:play()		--gem sound
	elseif soundon==true and nLevel==61 then
		if block.name=="1" or block.name=="2" or block.name=="3" or block.name=="4" or block.name=="5" or block.name=="6" or block.name=="7" or block.name=="8" or block.name=="9" then
			gemsound:play()		--gem sound
		end
	end

		-- level 1

		--script that trigger dialogs when big fish push the pipe in the first level
		if nLevel==1 and block.name=="steel-pipe-1x81" and pipepushed==false and timer>1 then
		
						
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.l1wowyoumovedit() end
		
						if whatwasthat:isPlaying() then whatwasthat:stop() end
						if icantgetsaidonce==true then icantgetthrough:stop() end
						if language=="en" then 
							if ihavenoidea:isPlaying() then ihavenoidea:stop() end 
						end
						
						

		pipepushed=true
		
		end
		--script that trigger dialogs when big fish touch the chair in the first level
		if nLevel==1 and block.name=="chair2" and chairtouched==false then
		
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.l1damnit() end
		
		if language=="en" and accent=="br" then
						whatwasthat:stop() 
						ihavenoidea:stop()
						weshouldgoandhave:stop()
						waitimgoingwithyou:stop()
					
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/en/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
		elseif language=="en" and accent=="us" then
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/en-us/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
		elseif language=="fr" then
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/fr/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
		elseif language=="es" then
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/es-la/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
					end
		end
		
		-- level 3
		
		--script when shortpipe is pushed by big fish on level 3
		if nLevel==3 and block.name=="shortpipe2" and shortpipepushed==false then
			if language=="en" then
		
						shortpipepushed=true
			elseif language=="fr" then
			
						shortpipepushed=true
			end
		end
		--script when big can is pushed by any fish on level 3
		if nLevel==3 and block.name=="strawberrymarmelade" and strawberrymarmeladepushed==false then
			if language=="en" then
		
						shortpipepushed=true
						strawberrymarmeladepushed=true
			
			elseif language=="fr" then
	
						thisistrickym= love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistrickym.ogg","stream" )
						thisistrickym:setEffect('myEffect')
						thisistrickym:play()
				
						shortpipepushed=true
						strawberrymarmeladepushed=true
			end			
			if language2=="nl" then
						
						thisistrickym= love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistrickym.ogg","stream" )
						thisistrickym:setEffect('myEffect')
						thisistrickym:play()
				
						shortpipepushed=true
						strawberrymarmeladepushed=true
			end
		end
		--script when big fish pushed long horizontal steal pipe on level 3
		if nLevel==3 and block.name=="pipelonghorizontal" and longpipehorizontalpushed==false then
			if language=="en" then
				if accent=="br" then
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/en/youarestandinginmyway/1youarestandingm.ogg","stream" )
						justwaitandsee= love.audio.newSource( "/externalassets/dialogs/level3/en/youarestandinginmyway/4justwaitandsee.ogg","stream" )

				elseif accent=="us" then
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/en-us/youarestandinginmyway/1youarestandingm.ogg","stream" )
						justwaitandsee= love.audio.newSource( "/externalassets/dialogs/level3/en-us/youarestandinginmyway/4justwaitandsee.ogg","stream" )
				end
				
			elseif language=="es" then
				if accent=="es" then
				elseif accent=="la" then
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/es-la/youarestandinginmyway/1youarestandingm.ogg","stream" )
						justwaitandsee= love.audio.newSource( "/externalassets/dialogs/level3/es-la/youarestandinginmyway/4justwaitandsee.ogg","stream" )
				end
				
			elseif language=="fr" then
			
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/fr/youarestandinginmyway/1youarestandingm.ogg","stream" )
						justwaitandsee= love.audio.newSource( "/externalassets/dialogs/level3/fr/youarestandinginmyway/4justwaitandsee.ogg","stream" )
			end
			
			if language2=="en" then
				if accent2=="br" then
							ifyoudropthat= love.audio.newSource( "/externalassets/dialogs/level3/en/youarestandinginmyway/2ifyoudropthat.ogg","stream" )
							here= love.audio.newSource( "/externalassets/dialogs/level3/en/youarestandinginmyway/3here.ogg","stream" )							
							
							
					elseif accent2=="us" then
							ifyoudropthat= love.audio.newSource( "/externalassets/dialogs/level3/en-us/youarestandinginmyway/2ifyoudropthat.ogg","stream" )
							here= love.audio.newSource( "/externalassets/dialogs/level3/en-us/youarestandinginmyway/3here.ogg","stream" )							
							
					end
			elseif language2=="pl" then
								
						ifyoudropthat= love.audio.newSource( "/externalassets/dialogs/level3/pl/youarestandinginmyway/2ifyoudropthat.ogg","stream" )
							here= love.audio.newSource( "/externalassets/dialogs/level3/pl/youarestandinginmyway/3here.ogg","stream" )	
						
						
			elseif language2=="es" then
					if accent2=="es" then
				elseif accent2=="la" then
						ifyoudropthat= love.audio.newSource( "/externalassets/dialogs/level3/es-la/youarestandinginmyway/2ifyoudropthat.ogg","stream" )
							here= love.audio.newSource( "/externalassets/dialogs/level3/es-la/youarestandinginmyway/3here.ogg","stream" )	
				end
			elseif language2=="nl" then
						ifyoudropthat= love.audio.newSource( "/externalassets/dialogs/level3/nl/youarestandinginmyway/2ifyoudropthat.ogg","stream" )
							here= love.audio.newSource( "/externalassets/dialogs/level3/nl/youarestandinginmyway/3here.ogg","stream" )
			end
			
							-- trigger subtitles
							timer=0
							stepdone=0
							--loadsubtitles()
							if talkies==true then Talkies.clearMessages() Obey.lev3uarestanding() end
				
			
			uarestanding=true
			--fish 1 effects
			youarestandingm:setEffect('myEffect')
			justwaitandsee:setEffect('myEffect')
			
			--fish 2
			ifyoudropthat:setEffect('myEffect')
			here:setEffect('myEffect')
			
			longpipehorizontalpushed=true
		end
		--script when any fish pushed the book on level 3
		if nLevel==3 and block.name=="booklvl3" and booklvl3pushed==false then
						
			if language=="en" then
				if accent=="br" then
						sothatwillcatchthesteel= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahint/2sothatwillcatchthesteel.ogg","stream" )
						whatifigotheupperway= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahint/4whatifigotheupperway.ogg","stream" )
				elseif accent=="us" then
						sothatwillcatchthesteel= love.audio.newSource( "/externalassets/dialogs/level3/en-us/wewillgiveyouahint/2sothatwillcatchthesteel.ogg","stream" )
						whatifigotheupperway= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahint/4whatifigotheupperway.ogg","stream" )
				end
				
			elseif language=="es" then
				if accent=="es" then
				
				elseif accent=="la" then
					sothatwillcatchthesteel= love.audio.newSource( "/externalassets/dialogs/level3/es-la/wewillgiveyouahint/2sothatwillcatchthesteel.ogg","stream" )
					whatifigotheupperway= love.audio.newSource( "/externalassets/dialogs/level3/es-la/wewillgiveyouahint/4whatifigotheupperway.ogg","stream" )
				end
								
			elseif language=="fr" then
					sothatwillcatchthesteel= love.audio.newSource( "/externalassets/dialogs/level3/fr/wewillgiveyouahint/2sothatwillcatchthesteel.ogg","stream" )
					whatifigotheupperway= love.audio.newSource( "/externalassets/dialogs/level3/fr/wewillgiveyouahint/4whatifigotheupperway.ogg","stream" )
			end

			if language2=="en" then
				if accent2=="br" then
						wewillgiveyouahint= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahint/1wewillgiveyouahint.ogg","stream" )
						butwewillnottellyou= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahint/3butwewillnottellyou.ogg","stream" )
						shh= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahint/5shh.ogg","stream" )
						
				elseif accent2=="us" then
						wewillgiveyouahint= love.audio.newSource( "/externalassets/dialogs/level3/en-us/wewillgiveyouahint/1wewillgiveyouahint.ogg","stream" )
						butwewillnottellyou= love.audio.newSource( "/externalassets/dialogs/level3/en-us/wewillgiveyouahint/3butwewillnottellyou.ogg","stream" )
						shh= love.audio.newSource( "/externalassets/dialogs/level3/en-us/wewillgiveyouahint/5shh.ogg","stream" )
				end
			elseif language2=="es" then
					if accent2=="es" then
				elseif accent2=="la" then
						wewillgiveyouahint= love.audio.newSource( "/externalassets/dialogs/level3/es-la/wewillgiveyouahint/1wewillgiveyouahint.ogg","stream" )
						butwewillnottellyou= love.audio.newSource( "/externalassets/dialogs/level3/es-la/wewillgiveyouahint/3butwewillnottellyou.ogg","stream" )
						shh= love.audio.newSource( "/externalassets/dialogs/level3/es-la/wewillgiveyouahint/5shh.ogg","stream" )
					end
			elseif language2=="pl" then
					
						wewillgiveyouahint= love.audio.newSource( "/externalassets/dialogs/level3/pl/wewillgiveyouahint/1wewillgiveyouahint.ogg","stream" )
						butwewillnottellyou= love.audio.newSource( "/externalassets/dialogs/level3/pl/wewillgiveyouahint/3butwewillnottellyou.ogg","stream" )
						shh= love.audio.newSource( "/externalassets/dialogs/level3/pl/wewillgiveyouahint/5shh.ogg","stream" )
			elseif language2=="nl" then
					
						wewillgiveyouahint= love.audio.newSource( "/externalassets/dialogs/level3/nl/wewillgiveyouahint/1wewillgiveyouahint.ogg","stream" )
						butwewillnottellyou= love.audio.newSource( "/externalassets/dialogs/level3/nl/wewillgiveyouahint/3butwewillnottellyou.ogg","stream" )
						shh= love.audio.newSource( "/externalassets/dialogs/level3/nl/wewillgiveyouahint/5shh.ogg","stream" )
			end
			
			-- trigger subtitles
							timer=0
							stepdone=0
							--loadsubtitles()
							if talkies==true then Talkies.clearMessages() Obey.lev3wewillgiveyouahint() end
			
			booklvl3pushed=true
		end
		
		-- level 7
		
			--script when any fish pushed matress on level 7
		if nLevel==7 and block.name=="matress" and matresspushed==false then
							
			matresspushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev7part2() end
		end
		
		if nLevel==27 and block.name=="door" and dooropened==false then
					
							dooropened=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev27door() end
		end
				
		
		-- level 13
		--[[
		if nLevel==13 and block.name=="skull" and skullpushed==false then
		
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev13skull() end
		end
		
		-- level 20
		
		if nLevel==20 and block.name=="skull" and skullpushed==false then
		
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							
							canyouallmind = love.audio.newSource( "/externalassets/dialogs/level20/en/canyouallmind.ogg","stream" )
							canyouallmind:setEffect('myEffect')
							canyouallmind:play()
							
							if talkies==true then Talkies.clearMessages() Obey.lev20skull() end
		end
		
		-- level 28
		
				if nLevel==28 and block.name=="skull" and skullpushed==false then
		
		
							curiouspeople = love.audio.newSource( "/externalassets/dialogs/level28/en/curiouspeople.ogg","stream" )
							curiouspeople:setEffect('myEffect')
							curiouspeople:play()
		
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev28skull() end
		end
		--]]
		if nLevel==30 and block.name=="crab" and crabtouched==false then
					
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev30crab() end
	
						crabtouched=true
									
		end
		--[[
		-- level 45
		
			if nLevel==45 and block.name=="skull" and skullpushed==false then
		
							skullpushed=true
							
							welliwasasleep = love.audio.newSource( "/externalassets/dialogs/level45/en/welliwasasleep.ogg","stream" )
							welliwasasleep:play()
							
							
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev45skull() end
		end
		--]]
		-- sound for the magnet on level 53
			if nLevel==53 and block.name=="magnet" and magnetpushed==false then
					electromagnetfield = love.audio.newSource( "/externalassets/sounds/level53/electromagnetfield.ogg","stream" )
					electromagnetfield:play()
					magnetpushed=true
		 elseif nLevel==53 and block.name=="radio" and radiopushed==false then
					alienradio = love.audio.newSource( "/externalassets/sounds/level53/alienradio.ogg","stream" )
					alienradio:play()
					radiopushed=true
		end
		
		if nLevel==54 then
				if enginepushed==false then			-- if the engine is off
					if block.name=="enginekey" and positionalienenginey==positionenginekeyy-2 then	-- if the key is pushed while the engine is off	and they are in the same y coordinate
					
						if thislooks:isPlaying() then thislooks:stop() end 
			
						engineon= love.audio.newSource( "/externalassets/dialogs/level54/engineon.ogg","stream" )
						engineon:setEffect('myEffect')
						engineon:play()
						
						engineon2= love.audio.newSource( "/externalassets/dialogs/level54/engineon2.ogg","stream" )
						engineon2:setEffect('myEffect')
						engineon2:play()
						
							
							UnderwaterSynthesisedLowClicky = love.audio.newSource("externalassets/sounds/atmosphere/AtmosDeepRumblyUnderwaterSynthesisedLowClicky.ogg","stream" )
							UnderwaterSynthesisedLowClicky:play()
				
						enginepushed=true				-- then engine have just been turn on
						engineononce=true
						
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev54engineon() end
						
					end
				elseif enginepushed==true then			-- if the engine is on
					if block.name=="enginekey" or block.name=="alienengine" then	-- if the key or engine is moved
		
						if (positionalienenginex-positionenginekeyx)>2 then		-- if the key of the engine is separated by 1 blocks, the engine will turn off
							engineon:stop()
							engineon2:stop()
							
							whatareyoudoing:stop()
							whathaveyouactivated:stop()
							whathaveyoudone:stop()
							thisisterrible:stop()
							mayday:stop()
							howcani:stop()
							
							enginepushed=false
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev54engineoff() end
						
							engineoff= love.audio.newSource( "/externalassets/dialogs/level54/engineoff.ogg","stream" )
							engineoff:setEffect('myEffect')
							engineoff:play()
						
							if language=="en" then
									-- trigger subtitles
									timer=0
									stepdone=0
									if talkies==true then Talkies.clearMessages() Obey.lev54engineoff() end
							elseif language=="fr" then
										-- trigger subtitles
									timer=0
									stepdone=0
									if talkies==true then Talkies.clearMessages() Obey.lev54engineoff() end
							end
						end
				end
			end
				
		elseif nLevel==54 and block.name=="enginekey" and enginepushed==true then	-- if the engine is on
		
						if (positionalienenginex-positionenginekeyx)>2 then		-- if the key of the engine is separated by 3 blocks, the engine will turn off
							engineon:stop()
							engineon2:stop()
							
							if icantakeitout:isPlaying() then icantakeitout:stop() end
							--if :isPlaying() then :stop() end
							enginepushed=false
							
							engineoff= love.audio.newSource( "/externalassets/dialogs/level54/engineoff.ogg","stream" )
							engineoff:setEffect('myEffect')
							engineoff:play()
						
							
							if language=="en" then
								finally= love.audio.newSource( "/externalassets/dialogs/level54/finally.ogg","stream" )
								finally:setEffect('myEffect')
								finally:play()
							elseif language=="fr" then
								finally= love.audio.newSource( "/externalassets/dialogs/level54/fr/finally.ogg","stream" )
								finally:setEffect('myEffect')
								finally:play()
							end

						end
			
			
			-- level 55 Nothing but steel
			elseif nLevel==55 and block.name=="pipe3triggeralarm" and pipepushed==false and timer>2 then
					pipepushed=true
					 --= love.audio.newSource("externalassets/.ogg","stream" )
					--love.audio.play( Hypnotic_Puzzle )
					--Hypnotic_Puzzle :setVolume(0.2)
		
					lovebpmload("externalassets/dialogs/level55/alarmloop.ogg")
					--music:setBPM(127)
					music:play()
					music:setVolume(0.4)
		
			
			-- level 56
		
			elseif nLevel==56 and lightswitchon==false and lightswitchpushedafterfalling==false and timer>5 then
					if block.name=="lightswitch" then
					
							lightswitchon=true
							lightswitchpushedafterfalling=true
							
							switchonsound:setEffect('myEffect')
							switchonsound:play()
							
							 --trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() 
							Obey.lev56switchon()
							end
							
								if wait:isPlaying() then wait:stop() end
					
					end
		
		-- level 58 The real propulsion
		
			elseif nLevel==58 and block.name=="alien1" or block.name=="alien2"  then
									
								alienlanguage= love.audio.newSource( "/externalassets/sounds/level58/Alien_Language_00.ogg","stream" )
								alienlanguage:setEffect('myEffect')
								alienlanguage:play()
	
		-- level 59
		
			elseif nLevel==59 and skullpushed==false then
					if block.name=="skullbottom" then
					--if block.name=="skullupleft" or block.name=="skullupleft+" or block.name=="skullupright" or block.name=="skullmidright" or block.name=="skullbottom" then
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							anothernight = love.audio.newSource( "/externalassets/dialogs/extras/skulls/anothernight.ogg","stream" )
							anothernight:setEffect('myEffect')
							anothernight:play()
							if talkies==true then Talkies.clearMessages() end
					end
					--if block.name=="skullupleft" then if talkies==true then Obey.lev59skullupleft() end end
					--if block.name=="skullupleft+" then if talkies==true then Obey.lev59skullupleftplus() end end
					if block.name=="skullupright" then if talkies==true then Obey.lev59skullupright() end end
					--if block.name=="skullmidright" then if talkies==true then Obey.lev59skullmidright() end end
					if block.name=="skullbottom" then if talkies==true then Obey.lev59skullbottom() end end
			
	
			end
	end


function eventsblockfalling(block)
	-- sure falling
				if soundon==true and block.heavy==true then
				    impactmetal:play()		--impact sound
				elseif soundon==true and block.heavy==false then
					impactgolf:play()		--impact sound low
				end
		
				-- Pipe falling level 1
				if nLevel==1 and block.name=="steel-pipe-1x81" and pipefalled==false then
							pipefalled=true
								-- trigger subtitles
							timer=0
							stepdone=0
							loadsubtitles()
							loadtouchtext()		-- reload the touch interface
							if talkies==true then Talkies.clearMessages() Obey.lev1() end
				end
		
				-- Sound briefcase level 2
				if nLevel==2 and block.name=="trunk" and briefcaseclosed==true then
					require("game/levels/briefcasemessage")
			
								-- trigger subtitles
							timer=0
							stepdone=0
							loadsubtitles()
							optionsload()		-- reload the values for the languages on the menus
							loadtouchtext()		-- reload the touch interface
							
							if talkies==true then Talkies.clearMessages() Obey.lev2briefcase() end
							
							gamestatus="briefcasemessage"
							briefcaseload()
				end

				-- zeus statue falling level 23
				
					if nLevel==23 and block.name=="zeus" and zeusfalled==false then
					zeusfalled=true
								-- trigger subtitles
							timer=0
							stepdone=0
							--loadsubtitles()
							--optionsload()		-- reload the values for the languages on the menus
							--loadtouchtext()		-- reload the touch interface
							if talkies==true then Talkies.clearMessages() Obey.lev23zeus() end
				end
				
				-- level 56 guarded corridor
				if nLevel==56 and block.name=="lightswitch" and lightswitchon==true and lightonvoiceplayed==false then
					
						
						switchonsound:setEffect('myEffect')
						switchonsound:play()
						
								-- trigger subtitles
									if talkies==true then 
									Talkies.clearMessages()
									timer=0
									stepdone=0
									Obey.lev56switchoff()
							end
							
							lightswitchon=false
							lightonvoiceplayed=true
				end
				
				-- Sound crystals level 61
					if soundon==true and nLevel==60 then
						gemsound2:play()		--gem sound
				elseif soundon==true and nLevel==61 then
					if block.name=="1" or block.name=="2" or block.name=="3" or block.name=="4" or block.name=="5" or block.name=="6" or block.name=="7" or block.name=="8" or block.name=="9" then
						gemsound2:play()		--gem sound
					end
				end

				if nLevel==30 and block.name=="crab" and crabtouched==true and crabfalled==false then
					whowokemeup:stop()
					leavemealone:stop()
					whatdoyouwant:stop()
					donttouchme:stop()
						
						-- trigger subtitles
									if talkies==true then 
									Talkies.clearMessages()
									timer=0
									stepdone=0
									Obey.lev30crabups()
							end
					
					
					if language=="en" then 
						if accent=="br" then
							ups = love.audio.newSource( "/externalassets/dialogs/level30/en-us/crab/5ups.ogg","stream" )
						elseif accent=="us" then
							ups = love.audio.newSource( "/externalassets/dialogs/level30/en-us/crab/5ups.ogg","stream" )
						end
					--[[elseif language=="de" then
						ups = love.audio.newSource( "/externalassets/dialogs/level30/de/5ups.ogg","stream" )
					elseif language=="fr" then
						ups = love.audio.newSource( "/externalassets/dialogs/level30/fr/tups.ogg","stream" )
						--]]
					elseif language=="ru" then
						ups = love.audio.newSource( "/externalassets/dialogs/level30/ru/crab/5ups.ogg","stream" )
					end
					if language2=="pl" then
						ups = love.audio.newSource( "/externalassets/dialogs/level30/pl/crab/5ups.ogg","stream" )
					end
					ups:setEffect('myEffect')
					ups:play()
					crabfalled=true
			
end
end
