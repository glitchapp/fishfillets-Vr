--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

Some guidelines to set up Vr tracking and inputs written assisted by chatGPT

--]]

require ('levels/prepareMap')	-- Importing the 'push-blocks' module
require ('logic/levelcompleted')	-- Importing the 'push-blocks' module
require ('logic/events')	-- Importing the 'push-blocks' module
local pb = require ('logic/push-blocks')	-- Importing the 'push-blocks' module
require ("game/savegame")	-- Loading level menu module
savegameload()

function lovr.load()

timer=0
nLevel=1


--debugmode=true
-- Initialize VR
    --lovr.headset.init()
    --lovr.headset.create()
    
    --[[
    -- Check if VR headset is present
    local driver = lovr.headset.getDriver()
    
    if driver then
        lovr.headset.init()
    else
        print("No VR headset detected. Running in non-VR mode.")
        -- Initialize your application for non-VR mode here
    end
	--]]
    -- Other initialization code
    
--[[
    -- Load other assets and setup your game
models = {
    left = lovr.headset.newModel('assets/hands/left.glb'),
    right = lovr.headset.newModel('assets/hands/right.glb')
}
--]]

 -- Example: Initialize variables for controllers
    controllers = {}
    
    -- Example: Load VR controllers
    --controller1 = lovr.headset.newController(1)
    --controller2 = lovr.headset.newController(2)
    
    
    
    

varelaroundfont = lovr.graphics.newFont(12,2,2.0)	-- Creating a new font
lovrload()	-- Calling the 'lovrload' function

require ('levels/loadlevelassets')		-- Loading level assets
loadlevelassets()						-- Calling the 'loadlevelassets' function
require ('logic/drawassets')			-- Loading drawing assets

gamestatus="game"	-- Setting the initial game status
--gamestatus="levelselection"	-- Setting the initial game status

	player1tex=lovr.graphics.newTexture("assets/players/fish-4x2.png")	-- Loading player 1 texture
	player2tex=lovr.graphics.newTexture("assets/players/fish-3x1.png")	-- Loading player 2 texture
	player1mat=lovr.graphics.newMaterial(player1tex,1,1,1,1)			-- Creating a material using player 1 texture
	player2mat=lovr.graphics.newMaterial(player2tex,1,1,1,1)			-- Creating a material using player 2 texture

	-- Loading 3D player models
	player1_3d=lovr.graphics.newModel('/externalassets/players/fish1/fish1_new.glb')
	player2_3d=lovr.graphics.newModel('/externalassets/players/fish2/fish2_new.glb')


require ("game/inputs")	-- Loading input handling module
lovr.mouse = require ("lib/mousevr/lovr-mouse")	-- Loading mouse input module
require ("game/levelmenu")	-- Loading level menu module
levelload()	-- Calling the 'levelload' function



    lovr.graphics.setBackgroundColor(0.24, 0.82, .82)	-- Setting the background color

end


function lovr.update(dt)

 -- Update VR input
    lovr.headset.update(dt)

	--[[
    -- Retrieve a table of connected controllers
    local connectedControllers = lovr.headset.getControllers()

    for i, controller in ipairs(connectedControllers) do
        -- Handle input for each connected controller
        if controller:isDown('trigger') then
            -- Handle trigger press for this controller
        end
        -- Handle other input as needed
    end
--]]

    if gamestatus == "game" then -- If game is in progress
        if dt > 0.05 then
            dt = 0.05 -- Limiting the maximum delta time
        end
        if currentplayer == "1" then
            pb:update(dt) -- Updating push blocks module
        end
    elseif gamestatus == "levelselection" then
        -- levelupdate(dt) -- Uncomment or add your logic for level update
    elseif gamestatus == "levelcompleted" then
		timer=timer+dt
		if timer>3 then 

			loadlevelassets()
			gamestatus="game"
		end	-- if timer>3 go to the next level
    
    end
    -- print(lovr.mouse.getPosition()) -- Printing mouse position
end

function lovr.draw(dt,pass)

	--[[-- hand drawing
  for hand, model in pairs(models) do
    if lovr.headset.isTracked(hand) then
      pass:draw(model, mat4(lovr.headset.getPose(hand)))
    end
  end
	--]]
--gamestatus="levelcompleted"
	if gamestatus=="game" then	-- If game is in progress
	
		mydraw()	-- Calling the 'mydraw' function
		lovr.graphics.setColor(1,1,1,1)	-- Setting the drawing color to white
	 -- Set the shader
  --lovr.graphics.setShader(shader)
		
				if nLevel==1 then	-- Checking the current level
				lovr.graphics.plane (level1bck2mat, 750, 720 ,50, 1450,850)	-- Drawing a plane with specified material and dimensions
				lovr.graphics.skybox(level1bck3tex)	-- Drawing a skybox with specified texture
			elseif nLevel==2 then	-- Checking the current level
				lovr.graphics.plane (level2bck1mat, 1550, 1300 ,50, 3200,2600)	-- Drawing a plane with specified material and dimensions
				lovr.graphics.skybox(level2bck2tex)	-- Drawing a skybox with specified texture
			elseif nLevel==3 then 
				lovr.graphics.plane (level3bckmat, 750, 720 ,50, 3840,2160)
				lovr.graphics.skybox(level3bck2tex)
			elseif nLevel==4 then
				lovr.graphics.plane (level4bckmat, 550, 900 ,50, 1200,2100)
				lovr.graphics.skybox(level4bck2tex)
			elseif nLevel==5 then
				lovr.graphics.plane (level5bckmat, 1550, 700 ,50, 3200,1700)
				lovr.graphics.skybox(level5bck2tex)
			elseif nLevel==6 then
				lovr.graphics.plane (level6bckmat, 850, 700 ,50, 2000,1700)
				lovr.graphics.skybox(level6bck2tex)
			elseif nLevel==7 then lovr.graphics.skybox(level7bck2tex)
			elseif nLevel==8 then
				lovr.graphics.plane (level8bckmat, 550, 600 ,50, 1500,1300)
				lovr.graphics.skybox(level8bck2tex)
			elseif nLevel==9 then lovr.graphics.skybox(level9bck2tex)
			elseif nLevel==10 then lovr.graphics.skybox(level10bck2tex)
			elseif nLevel==10 then lovr.graphics.skybox(level10bck2tex)
			elseif nLevel==11 then
				lovr.graphics.plane (level11bckmat, 750, 720 ,50, 3840,2160)
				lovr.graphics.skybox(level11bcktex)
			elseif nLevel==12 then lovr.graphics.skybox(level12bcktex)
			--Galleon:draw(500,600,0)
			
				elseif nLevel==13 then lovr.graphics.skybox(level13bcktex)
				elseif nLevel==14 then lovr.graphics.skybox(level14bcktex)
				lovr.graphics.plane (level14bckmat, 1400, 620 ,50, 2800,1300)
				
				elseif nLevel==15 then lovr.graphics.skybox(level15bcktex)
				lovr.graphics.plane (level15bckmat, 1300, 500 ,50, 1500,800)
				elseif nLevel==16 then lovr.graphics.skybox(level16bcktex)
				lovr.graphics.plane (level16bckmat,  1700, 1000 ,50, 3500,2000)
				elseif nLevel==17 then lovr.graphics.skybox(level17bcktex)
				elseif nLevel==18 then lovr.graphics.skybox(level18bcktex)
				elseif nLevel==19 then lovr.graphics.setColor(1,0,1,0.8)  lovr.graphics.skybox(level19bcktex)
				elseif nLevel==20 then lovr.graphics.skybox(level20bcktex)
				elseif nLevel==21 then lovr.graphics.skybox(level21bcktex)
				elseif nLevel==22 then lovr.graphics.skybox(level22bcktex)
				elseif nLevel==23 then lovr.graphics.skybox(level23bcktex)
				elseif nLevel==24 then lovr.graphics.skybox(level24bcktex)
				elseif nLevel==25 then lovr.graphics.skybox(level25bcktex)
				lovr.graphics.plane (level25bck2mat, 1050, 750 ,50, 2000,1300)
				elseif nLevel==26 then lovr.graphics.skybox(level26bcktex)
				elseif nLevel==27 then lovr.graphics.skybox(level27bcktex)
				elseif nLevel==28 then lovr.graphics.skybox(level28bcktex)
				elseif nLevel==29 then lovr.graphics.setColor(1,0,0.5,0.94)	lovr.graphics.skybox(level29bcktex)
			elseif nLevel==30 then lovr.graphics.skybox(level30bcktex)
			elseif nLevel==31 then lovr.graphics.skybox(level31bcktex)
			elseif nLevel==32 then lovr.graphics.skybox(level32bcktex)
			elseif nLevel==33 then lovr.graphics.skybox(level33bcktex)
			elseif nLevel==34 then lovr.graphics.skybox(level34bcktex)
			elseif nLevel==35 then lovr.graphics.skybox(level35bcktex)
			elseif nLevel==36 then lovr.graphics.skybox(level36bcktex)
			elseif nLevel==37 then lovr.graphics.skybox(level37bcktex)
			elseif nLevel==38 then lovr.graphics.skybox(level38bcktex)
			elseif nLevel==39 then lovr.graphics.setColor(1,0,0,0.94)	lovr.graphics.skybox(level39bcktex)
			elseif nLevel==40 then lovr.graphics.setColor(1,0,1,0.94)	lovr.graphics.skybox(level40bcktex)
			elseif nLevel==41 then lovr.graphics.setColor(0,0,1,0.94)	lovr.graphics.skybox(level41bcktex)
			elseif nLevel==42 then lovr.graphics.setColor(1,0.5,1,0.94)	lovr.graphics.skybox(level42bcktex)
			elseif nLevel==43 then lovr.graphics.setColor(1,0.5,0,0.94)	lovr.graphics.skybox(level43bcktex)
			elseif nLevel==44 then lovr.graphics.setColor(1,0.8,1,0.94)	lovr.graphics.skybox(level44bcktex)
			elseif nLevel==45 then lovr.graphics.skybox(level45bcktex)
			elseif nLevel==46 then lovr.graphics.skybox(level46bcktex)
			elseif nLevel==47 then lovr.graphics.skybox(level47bcktex)
			elseif nLevel==48 then lovr.graphics.skybox(level48bcktex)
			elseif nLevel==49 then lovr.graphics.skybox(level49bcktex)
			elseif nLevel==50 then lovr.graphics.skybox(level50bcktex)
			elseif nLevel==51 then lovr.graphics.skybox(level51bcktex)
				
			elseif nLevel==52 then
				--lovr.graphics.plane (level52bckmat, 750, 720 ,50, 3840,2160)
			lovr.graphics.skybox(level52bcktex)
			elseif nLevel==53 then lovr.graphics.skybox(level53bcktex)
			elseif nLevel==54 then lovr.graphics.setColor(0,1,0,1)	lovr.graphics.skybox(level54bcktex)
			elseif nLevel==55 then lovr.graphics.setColor(1,0,0,1)	lovr.graphics.skybox(level55bcktex)
			elseif nLevel==56 then lovr.graphics.setColor(1,0,1,1)	lovr.graphics.skybox(level56bcktex)
			elseif nLevel==57 then lovr.graphics.setColor(1,1,0,1)	lovr.graphics.skybox(level57bcktex)
			elseif nLevel==58 then lovr.graphics.setColor(0,1,1,1)	lovr.graphics.skybox(level58bcktex)
			elseif nLevel==59 then lovr.graphics.skybox(level59bcktex)
			elseif nLevel==60 then lovr.graphics.skybox(level60bcktex)
			elseif nLevel==61 then lovr.graphics.skybox(level61bcktex)
			elseif nLevel==62 then lovr.graphics.skybox(level62bcktex)
			elseif nLevel==63 then lovr.graphics.skybox(level63bcktex)
			elseif nLevel==64 then lovr.graphics.setColor(1,0,1,1)	lovr.graphics.skybox(level64bcktex)
			elseif nLevel==65 then lovr.graphics.skybox(level65bcktex)
			elseif nLevel==66 then lovr.graphics.skybox(level66bcktex)
			elseif nLevel==67 then lovr.graphics.skybox(level67bcktex)
			elseif nLevel==68 then lovr.graphics.skybox(level68bcktex)
			elseif nLevel==69 then lovr.graphics.skybox(level69bcktex)
			elseif nLevel==70 then lovr.graphics.skybox(level70bcktex)
			elseif nLevel==71 then lovr.graphics.skybox(level71bcktex)
			elseif nLevel==72 then lovr.graphics.skybox(level72bcktex)
			elseif nLevel==73 then lovr.graphics.skybox(level73bcktex)
			elseif nLevel==74 then lovr.graphics.skybox(level74bcktex)
			elseif nLevel==75 then lovr.graphics.skybox(level75bcktex)
			elseif nLevel==76 then lovr.graphics.skybox(level76bcktex)
			elseif nLevel==77 then lovr.graphics.skybox(level77bcktex)
			elseif nLevel==78 then lovr.graphics.skybox(level78bcktex)
			elseif nLevel==79 then lovr.graphics.skybox(level79bcktex)
			end
	--lovr.graphics.setShader()
	elseif gamestatus=="levelselection" then
		leveldraw()
	elseif gamestatus=="levelcompleted" then
	--debug
	--print(timer)
	--print(nLevel)
	--print(gamestatus)
		--lovr.graphics.setColor(1,1,1,1)	-- Setting the drawing color to white
		mydraw()
		lovr.graphics.print("Level completed",	750, 520 ,-100 ,100,1800,100)
		lovr.graphics.print("Well done!",		750, 600 ,-100, 100,1800,100)
		--lovr.graphics.print("New level starts in " .. timer-3 .. "seconds",	750, 700 ,-100, 100,1800,100)
	end


end






