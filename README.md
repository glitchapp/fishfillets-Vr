# Fish Fillets remake Vr
Port of Fish Fillets remake project for Vr

![Screenshot 1](/screenshots/screenshot2.webp)

# 0. Releases

Available releases can be downloaded [![here](https://codeberg.org/glitchapp/fishfillets-Vr/releases)]

# 1. Development status:

The game is currently playable and you should be able to complete the whole game.

Not implemented yet:

Vr controls (the game is playable with keyboard yet)
Load / save functions (workaround till the functions are built: if you don't want to start from level 1 change nLevel variable in main.lua)

Settings
========

To permantly change global options please edit config.lua

Controls
========
Keyboard:
Use arrows or WASD to move the current fish.
Use space bar to switch fish.

# Credits

	[![Credits](https://codeberg.org/glitchapp/fishfillets-Vr/src/branch/main/credits.md)]

## Contributing

I'm looking for help in the following areas:

- Development
  - Unresolved issues will be posted on issues, pull request to solve them are welcome, if an issue is not opened please open it first so that the provided solution can be explained and dicussed first.

- Modeling and texturing
  - 3d assets for the objects, characters and backgrounds
  
- Testing
  - Betatesting, reporting bugs and providing feedback helps to improve this software.

- Feedback
  - Feedback regarding any part of the game is welcome and helpful, feel free to comment on any aspect you want to be improved.

# License

 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

The libraries on the lib folder belong to their authors and have their own licenses which can be found on their folders.

# Donations

If you like this remake and you want to support the development - please consider to donate. Any donations are greatly appreciated.

![Zcash address:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/Zc.webp) t1h9UejxbLwJjCy1CnpbYpizScFBGts5J3N
![Zcash Qr Code:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/ZCashQr160.webp)
