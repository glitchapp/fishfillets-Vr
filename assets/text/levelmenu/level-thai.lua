zonetext={"บ้านปลา",
"Ship Wrecks",
"Silver's Ship",
"City In the Deep",
"UFO",
"Coral reef",
"Treasure Cave",
"Dump",
"Secret Computer",
"NG",
"Branch of the New Generation",
}
fishhouse0={"มันเริ่มต้นอย่างไร",		--1
"ข้อความกระเป๋าเอกสาร",				--2
"ซ้อมในห้องใต้ดิน",						--3
"ห้องสมุด Flotsam",					--4
"พืชบนบันได",							--5
"ระเบียบในห้องหม้อไอน้ำ",				--6
"ใต้แนวปะการัง",						--7
"ปิดในตู้เสื้อผ้า",						--8
}

shipwrecks1={"Drowned Submarine",	--9
"Picnic Boat",						--10
"Great War",						--11
"The Ship of Captain Silver",		--12
"The Last Voyage",					--13
"Altitude: Minus 9000 Feet",		--14
"Bathyscaph",						--15
"Amphibious Tank",					--16
"Eight Vikings in a Boat",			--17
"Return from the Party",			--18
" The Gods Must Be Mad",			--19
}
silversship2={"The First Mate's Cabin",--45
"The Winter Mess Hall",				--46
"Fire!",							--47
"Ship Kitchen",						--48
"Second Mate's Cabin",				--49
"Captain's Cabin",					--50
"Silver's Hideout",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"House With an Elevator",			--20
"Welcome to Our City",				--21
"Independence Day",					--22
"The Columns",						--23
"Uneven Pavement",					--24
"Mr. Cheop's House",				--25
"A Bit of Music",					--26
"Crab Freak Show",					--27
"Another Elevator",					--28
"And How It Was",					--29
"",
"",
"",
"",
"",
}

ufo4={"Power Plant",				--52
"Strange Forces",					--53
"Brm... Brm...",					--54
"Nothing But Steel",				--55
"Guarded Corridor",					--56
"Biological Experiments",			--57
"The Real Propulsion",				--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"First Bizarre Things",	--30
"Labyrinth",						--31
"Imprisoned",						--32
"Closed Society",					--33
"Sleeping Creatures",				--34
"Cancan Crabs",						--35
" One More Pearl; Please!",			--36
"Telepathic Devil",					--37
}

treasurecave6={"Aztec Art Hall",	--59
"Shiny Cave-In",					--60
"Giant's Chest",					--61
"The Hall of Ali-Baba",				--62
"The Deepest Cave",					--63
"What Would King Arthur Say?",		--64
"",}

dump7={"The Deep Server",			--38
"Almost No Wall",					--39
"Plumbman's Refute",				--40
"Adventure With Pink Duckie",		--41
"Shredded Stickman",				--42
"Real Chaos",						--43
"Outraged Greenpeace",				--44
"",
"",
"",
}

secretcomputer8={"Tetris",			--65
"Emulator",							--66
"Garden of War",					--67
"Favorites",						--68
"A hardware problem",				--69
"Read only",						--70
}

secret9={"Electromagnet",			--71
"fdto",								--72
"hanoi",							--73
"hole",								--74
"key",								--75
"keys",								--76
"linux",							--77
"rotate",							--78
"rush",								--79
}
