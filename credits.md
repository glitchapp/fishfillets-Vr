
Fish fillets remake (2022)
==========================

- main developer:

Glitchapp <glitchapp@disroot.org>

- developer of the new game engine / game logic:

darkfrei <https://github.com/darkfrei>

- Digital art, new background design and new assets:

Rosie Krijgsman <https://krijgsvrouwehwalp.wixsite.com/darkintentarts>
Max Mint <https://krita-artists.org/u/maxmint/activity/portfolio>

To check the credits for all the actors involved, new open music tracks and new assets please check this file:

<https://codeberg.org/glitchapp/fish-fillets-remake-assets/src/branch/master/Credits.md>


Fish fillets next generation (2011)
===================================

Credits for the original game and linux port "Fish fillets Next generation" on which this game is based (including all the levels, scripts and classic assets):

https://fillets.sourceforge.net/

Ivo Danihelka <ivo@danihelka.net>
- main developer
- developed new game engine

Pavel Danihelka <fillets@danihelka.net>
- webmaster, level data, documentation

Mirek Olsak <mirek@olsak.net>
- many new levels, undo

Frederic Panico <fratloev@yahoo.fr>
- French translation

Ronny Standtke <Ronny.Standtke@gmx.de>
- German translation

Simone Cociancich <simone.coch@tin.it>
- Italian translation

Przemek Bojczuk <przemek@bojczuk.pl>
- Polish translation

César Catrián C. <ccatrian@eml.cc>
- Spanish translation

Astrid de Wijn <A.S.deWijn@phys.uu.nl>
- Dutch translation

Damyan Ivanov <divanov@creditreform.bg>
- Bulgarian translation

Olov Gustavsson <olov.gustavsson@bredband.net>
- Swedish translation

Marko Burjek <email4marko@gmail.com>
- Slovenian translation

Ricardo Faria <rickybrag@gmail.com>
- Brazilian Portuguese translation

Leonid Myravjev <asm@asm.pp.ru>
- Russian translation

ALTAR interactive <info@altarinteractive.com>
- developed original game in 1998 (for Win32)
- released code and data under GNU/GPL in 03/2004
